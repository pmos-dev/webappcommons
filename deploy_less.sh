#!/bin/bash
cd less
tsc deploy.ts --lib es2015 --types "@types/node" && node deploy.js > palette.less
rm deploy.js
cd ..

for d in projects/*/ ; do
	mkdir -p "$d"src/lib/less/common
	rsync -av --exclude "node_modules" --exclude "deploy.ts" --delete "less/" "$d"src/lib/less/common/
done

mkdir -p "/home/pete/Dev/git/angular/cordovaangularcommons/projects/ngx-cordovaangularcommons-camera/src/lib/less/common"
rsync -av --exclude "node_modules" --exclude "deploy.ts" --delete "less/" /home/pete/Dev/git/angular/cordovaangularcommons/projects/ngx-cordovaangularcommons-camera/src/lib/less/common/

