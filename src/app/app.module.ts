import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HammerGestureConfig, HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';

import { NgxAngularCommonsCoreModule } from 'ngx-angularcommons-core';
import { NgxAngularCommonsBrowserModule } from 'ngx-angularcommons-browser';
import { NgxAngularCommonsAppModule } from 'ngx-angularcommons-app';
import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsGraphicsModule } from 'ngx-webappcommons-graphics';
import { NgxWebAppCommonsFormModule } from 'ngx-webappcommons-form';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';
import { NgxWebAppCommonsTableModule } from 'ngx-webappcommons-table';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

export class MyHammerConfig extends HammerGestureConfig {
	overrides = {
		// override hammerjs default configuration
		swipe: {
			direction: 31 // /! ugly hack to allow swipe in all direction
		},
		pan: {
			direction: 30 // /! ugly hack to allow pan in all direction
		},
		press: {
			time: 600
		}
	} as any;
}

@NgModule({
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		NgxAngularCommonsCoreModule.forRoot(),
		NgxAngularCommonsBrowserModule.forRoot(),
		NgxAngularCommonsAppModule.forRoot(),
		NgxAngularCommonsPipeModule,
		NgxWebAppCommonsCoreModule.forRoot(),
		NgxWebAppCommonsAppModule.forRoot(),
		NgxWebAppCommonsGraphicsModule,
		NgxWebAppCommonsFormModule.forRoot(),
		NgxWebAppCommonsMobileModule.forRoot(),
		NgxWebAppCommonsTableModule.forRoot(),
		AppRoutingModule
	],
	declarations: [
		AppComponent
	],
	providers: [
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
