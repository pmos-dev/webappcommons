import { Component } from '@angular/core';

import { CommonsThemeService } from 'ngx-webappcommons-core';
import { EMaterialDesignIcons } from 'ngx-webappcommons-core';
import { ECommonsThemeColor } from 'ngx-webappcommons-core';
import { ECommonsThemeContrast } from 'ngx-webappcommons-core';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.less']
})
export class AppComponent {
	EMaterialDesignIcons = EMaterialDesignIcons;
	
	show: boolean = false;
	show2: boolean = false;
	rows: number = 10;
	from: number = 3;
	total: number = 11;
	
	constructor(
			private themeService: CommonsThemeService
	) {
	}
	
	ngOnInit(): void {
		setTimeout((): void => { this.show = true; }, 1000);
		setTimeout((): void => { this.show2 = true; }, 2000);
		setTimeout((): void => {
			this.themeService.setCurrentColor(ECommonsThemeColor.ALISE);
		}, 3000);
		setTimeout((): void => {
			this.themeService.setCurrentContrast(ECommonsThemeContrast.DARK);
		}, 4000);
	}
	
	ngAfterViewInit(): void {
	}
}
