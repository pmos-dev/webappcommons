import { CommonsFile } from 'nodecommons-file';

const themes: string[] = CommonsFile.listFilesWithExtensions('./themes', 'less')
		.map((file: string): string => file.replace(/\.less$/, ''));

const code: string[] = [ `@charset 'utf8';\n\n`, `@web-app-commons__themes: ${themes.join(', ')};\n` ];
for (const theme of themes) {
	code.push(`
/* ${theme} theme */
@import (less) './themes/${theme}.less';

@web-app-commons__theme-${theme}__palette__body--background--greyscale: greyscale(@theme-${theme}__palette__body--background);
@web-app-commons__theme-${theme}__palette__body--background--greyscale--invert: difference(@web-app-commons__theme-${theme}__palette__body--background--greyscale, #ffffff);
@web-app-commons__theme-${theme}__palette__body--text: mix(@web-app-commons__theme-${theme}__palette__body--background--greyscale--invert, @web-app-commons__theme-${theme}__palette__body--background--greyscale, 80%);
@web-app-commons__theme-${theme}__palette__body--text--invert: difference(@web-app-commons__theme-${theme}__palette__body--text, #ffffff);

@web-app-commons__theme-${theme}__palette__modal--background: fade(@web-app-commons__theme-${theme}__palette__body--background--greyscale--invert, 50%);
@web-app-commons__theme-${theme}__palette__modal--background--dark: fade(@web-app-commons__theme-${theme}__palette__body--background--greyscale, 50%);

@web-app-commons__theme-${theme}__palette__disabled: mix(@web-app-commons__theme-${theme}__palette__body--text, @web-app-commons__theme-${theme}__palette__body--background--greyscale, 30%);
@web-app-commons__theme-${theme}__palette__subtle: mix(@web-app-commons__theme-${theme}__palette__body--background--greyscale, @web-app-commons__theme-${theme}__palette__body--background--greyscale--invert, 60%);
@web-app-commons__theme-${theme}__palette__subtler: mix(@web-app-commons__theme-${theme}__palette__body--background--greyscale, @web-app-commons__theme-${theme}__palette__body--background--greyscale--invert, 75%);
@web-app-commons__theme-${theme}__palette__subtlerer: mix(@web-app-commons__theme-${theme}__palette__body--background--greyscale, @web-app-commons__theme-${theme}__palette__body--background--greyscale--invert,88%);
@web-app-commons__theme-${theme}__palette__subtlest: mix(@web-app-commons__theme-${theme}__palette__body--background--greyscale, @web-app-commons__theme-${theme}__palette__body--background--greyscale--invert, 95%);

@web-app-commons__theme-${theme}__palette__subtle--dark: mix(@web-app-commons__theme-${theme}__palette__body--background--greyscale, @web-app-commons__theme-${theme}__palette__body--background--greyscale--invert, 40%);
@web-app-commons__theme-${theme}__palette__subtler--dark: mix(@web-app-commons__theme-${theme}__palette__body--background--greyscale, @web-app-commons__theme-${theme}__palette__body--background--greyscale--invert, 25%);
@web-app-commons__theme-${theme}__palette__subtlest--dark: mix(@web-app-commons__theme-${theme}__palette__body--background--greyscale, @web-app-commons__theme-${theme}__palette__body--background--greyscale--invert, 5%);

@web-app-commons__theme-${theme}__palette__shadow: fade(@web-app-commons__theme-${theme}__palette__body--background--greyscale--invert, 50%);

@web-app-commons__theme-${theme}__palette__shadow--dark: fade(@web-app-commons__theme-${theme}__palette__body--background--greyscale, 50%);
`);
}

console.log(code.join('\n'));
