import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

import { CommonsBottomNavigationBarItemComponent } from '../components/commons-bottom-navigation-bar-item/commons-bottom-navigation-bar-item.component';

import { CommonsUiParentChildComponentService, ICommonsUiParentChildComponentServiceItem } from './commons-ui-parent-child-component.service';

interface IItem extends ICommonsUiParentChildComponentServiceItem<CommonsBottomNavigationBarItemComponent> {
		labelChangeEmitter: EventEmitter<string|undefined>;
}

@Injectable()
export class CommonsBottomNavigationBarService extends CommonsUiParentChildComponentService<CommonsBottomNavigationBarItemComponent, IItem> {
	protected buildItem(component: CommonsBottomNavigationBarItemComponent, name: string|undefined, visible: boolean): IItem {
		// https://github.com/palantir/tslint/issues/3586
		// tslint:disable:object-literal-sort-keys
		return {
				component: component,
				name: name,
				visible: visible,
				labelChangeEmitter: new EventEmitter<string|undefined>(true)
		};
		// tslint:enable:object-literal-sort-keys
	}
	
	public labelChangedObservable(component: CommonsBottomNavigationBarItemComponent): Observable<string|undefined> {
		const existing: IItem|undefined = this.getExistingByComponent(component);
		if (!existing) throw new Error('Bottom navigation bar item not added before observable for label called');

		return existing.labelChangeEmitter;
	}

	public setLabel(component: CommonsBottomNavigationBarItemComponent, label: string|undefined): void {
		const existing: IItem|undefined = this.getExistingByComponent(component);
		if (!existing) return;
		
		existing.labelChangeEmitter.emit(label);
	}

	public setLabelByName(name: string, label: string|undefined): void {
		const component: CommonsBottomNavigationBarItemComponent|undefined = this.getComponentByName(name);
		if (!component) return;
		
		return this.setLabel(component, label);
	}
}
