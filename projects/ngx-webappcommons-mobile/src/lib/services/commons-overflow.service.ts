import { Injectable, EventEmitter } from '@angular/core';

import { CommonsMenuService } from 'ngx-webappcommons-app';

import { ICommonsOverflowItem } from '../interfaces/icommons-overflow-item';

@Injectable()
export class CommonsOverflowService {
	private setSubject: EventEmitter<ICommonsOverflowItem[]> = new EventEmitter<ICommonsOverflowItem[]>(true);
	private showSubject: EventEmitter<void> = new EventEmitter<void>(true);
	private selectSubject: EventEmitter<ICommonsOverflowItem> = new EventEmitter<ICommonsOverflowItem>(true);
	
	constructor(
			private menuService: CommonsMenuService
	) {}

	public setObservable(): EventEmitter<ICommonsOverflowItem[]> {
		return this.setSubject;
	}

	public showObservable(): EventEmitter<void> {
		return this.showSubject;
	}

	public selectObservable(): EventEmitter<ICommonsOverflowItem> {
		return this.selectSubject;
	}

	public setMenuItems(items: ICommonsOverflowItem[]): void {
		this.setSubject.emit(items);
	}
	
	public clear(): void {
		this.setSubject.emit([]);
	}
	
	public open(): void {
		this.showSubject.emit();
	}

	public close(): void {
		this.menuService.hide('_overflow');
	}
	
	public select(item: ICommonsOverflowItem): void {
		this.selectSubject.emit(item);
	}
}
