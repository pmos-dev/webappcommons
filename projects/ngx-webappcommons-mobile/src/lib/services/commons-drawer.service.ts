import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable()
export class CommonsDrawerService {
	private showEmitter: EventEmitter<string> = new EventEmitter<string>(true);
	private hideEmitter: EventEmitter<string> = new EventEmitter<string>(true);
	private cancelledEmitter: EventEmitter<string> = new EventEmitter<string>(true);

	public show(name: string) {
		this.showEmitter.emit(name);
	}

	public hide(name: string) {
		this.hideEmitter.emit(name);
	}
	
	public cancelled(name: string) {
		this.cancelledEmitter.emit(name);
	}
	
	public showObservable(): Observable<string> {
		return this.showEmitter;
	}
	
	public hideObservable(): Observable<string> {
		return this.hideEmitter;
	}
	
	public cancelledObservable(): Observable<string> {
		return this.cancelledEmitter;
	}
}
