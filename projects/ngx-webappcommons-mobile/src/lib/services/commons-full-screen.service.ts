import { Injectable } from '@angular/core';

import { CommonsUpService } from 'ngx-webappcommons-app';

import { CommonsTabBarService } from './commons-tab-bar.service';

@Injectable()
export class CommonsFullScreenService {

	constructor(
			private upService: CommonsUpService,
			private tabService: CommonsTabBarService
	) {}

	public open(
			title: string,
			callback: () => void,
			tabbar?: string
	): void {
		this.upService.down(title, callback);
		
		if (tabbar) this.tabService.hide(tabbar);
	}

	public close(tabbar: string): void {
		this.tabService.show(tabbar);
	}
	
	public back(): void {
		this.upService.up();
	}
	
	public reset(): void {
		this.upService.reset();
	}
}
