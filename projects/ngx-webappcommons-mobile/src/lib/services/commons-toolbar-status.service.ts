import { Injectable } from '@angular/core';

import { CommonsToolbarStatusComponent } from '../components/commons-toolbar-status/commons-toolbar-status.component';

import { CommonsUiParentChildComponentService, ICommonsUiParentChildComponentServiceItem } from './commons-ui-parent-child-component.service';

interface IItem extends ICommonsUiParentChildComponentServiceItem<CommonsToolbarStatusComponent> {}

@Injectable()
export class CommonsToolbarStatusService extends CommonsUiParentChildComponentService<CommonsToolbarStatusComponent, IItem> {
	protected buildItem(component: CommonsToolbarStatusComponent, name: string|undefined, visible: boolean): IItem {
		// https://github.com/palantir/tslint/issues/3586
		// tslint:disable:object-literal-sort-keys
		return {
				component: component,
				name: name,
				visible: visible
		};
		// tslint:enable:object-literal-sort-keys
	}
}
