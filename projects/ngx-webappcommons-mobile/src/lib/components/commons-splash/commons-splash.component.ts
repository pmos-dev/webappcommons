import { Component, Input } from '@angular/core';

import { CommonsType } from 'tscommons-core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { EMaterialDesignIcons } from 'ngx-webappcommons-core';
import { ICommonsAvatar } from 'ngx-webappcommons-app';

@Component({
	selector: 'commons-splash',
	templateUrl: './commons-splash.component.html',
	styleUrls: ['./commons-splash.component.less']
})
export class CommonsSplashComponent extends CommonsComponent {
	EMaterialDesignIcons = EMaterialDesignIcons;

	@Input() background?: string;
	@Input() avatar?: string|ICommonsAvatar;	// backwards compatibility
	@Input() text?: string;
	@Input() fade?: boolean;
	@Input() useContentBackground: boolean = false;
	
	getUrlAvatar(): string|undefined {
		if (!this.avatar) return undefined;
		if (CommonsType.isString(this.avatar)) return this.avatar;
		if (CommonsType.hasPropertyString(this.avatar, 'image')) return this.avatar.image;
		
		return undefined;
	}
	
	isIconAvatar(): boolean {
		if (!this.avatar) return false;
		if (CommonsType.isString(this.avatar)) return false;
		
		if (CommonsType.hasPropertyString(this.avatar, 'icon')) return true;
		
		return false;
	}
	
	getAvatar(): ICommonsAvatar {
		if (!this.isIconAvatar()) throw new Error('Avatar is invalid');

		return this.avatar as ICommonsAvatar;
	}

	getIconSet(): EMaterialDesignIcons {
		return this.getAvatar().iconSet || EMaterialDesignIcons.MATERIALDESIGN;
	}
}
