import { Component, Input, HostListener } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsDrawerService } from '../../services/commons-drawer.service';

@Component({
	selector: '[commons-drawer-menu-item]',
	templateUrl: './commons-drawer-menu-item.component.html',
	styleUrls: ['./commons-drawer-menu-item.component.less']
})
export class CommonsDrawerMenuItemComponent extends CommonsComponent {
	@Input() disabled: boolean = false;
	@Input() autoclose: boolean = true;
	@Input() icon!: string;

	@HostListener('click') doClick() {
		if (!this.disabled && this.autoclose) this.doClose();
	}

	constructor(
		private drawerService: CommonsDrawerService
	) {
		super();
	}

	public doClose(): void {
		this.drawerService.hide('_drawermenu');
	}
}
