import { Component, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsBottomNavigationBarService } from '../../services/commons-bottom-navigation-bar.service';

@Component({
	selector: 'commons-bottom-navigation-bar',
	templateUrl: './commons-bottom-navigation-bar.component.html',
	styleUrls: ['./commons-bottom-navigation-bar.component.less']
})
export class CommonsBottomNavigationBarComponent extends CommonsComponent implements OnInit {
	itemCount: number = 0;
	
	constructor(
			private bottomNavigationBarService: CommonsBottomNavigationBarService
	) {
		super();
	}

	public ngOnInit(): void {
		this.subscribe(
				this.bottomNavigationBarService.itemCountChangedObservable(),
				(count: number): void => {
					this.itemCount = count;
				}
		);
	}
}
