import { Component, OnInit, OnDestroy, Input, HostBinding, HostListener } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsUpService } from 'ngx-webappcommons-app';
import { EMaterialDesignIcons } from 'ngx-webappcommons-core';

import { CommonsBottomNavigationBarService } from '../../services/commons-bottom-navigation-bar.service';

@Component({
		selector: '[commons-bottom-navigation-bar-item]',
		templateUrl: './commons-bottom-navigation-bar-item.component.html',
		styleUrls: ['./commons-bottom-navigation-bar-item.component.less']
})
export class CommonsBottomNavigationBarItemComponent extends CommonsComponent implements OnInit, OnDestroy {
	EMaterialDesignIcons = EMaterialDesignIcons;
	
	@HostBinding('attr.disabled') @Input() disabled: boolean|undefined = undefined;
	
	@Input() name?: string;
	@Input() icon: string|undefined;
	@Input() iconSet: string = EMaterialDesignIcons.MATERIALDESIGN;
	@Input() caption: string|undefined;
	@Input() label?: string;
	@Input() important: boolean = false;
	@Input() visible: boolean = true;
	
	itemCount: number = 0;
	
	@HostListener('click') resetUp() {
		this.upService.reset();
	}

	constructor(
			private upService: CommonsUpService,
			private bottomNavigationBarService: CommonsBottomNavigationBarService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();

		this.subscribe(
				this.bottomNavigationBarService.itemCountChangedObservable(),
				(count: number): void => {
					this.itemCount = count;
				}
		);
		
		this.bottomNavigationBarService.addItem(this, this.name, this.visible);

		// have to do this after it is added
		this.subscribe(
				this.bottomNavigationBarService.labelChangedObservable(this),
				(label: string|undefined): void => {
					this.label = label;
				}
		);
	}

	ngOnDestroy(): void {
		super.ngOnDestroy();
		
		this.bottomNavigationBarService.removeItem(this);
	}

}
