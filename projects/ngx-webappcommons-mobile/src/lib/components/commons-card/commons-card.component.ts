import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: '[commons-card]',
	templateUrl: './commons-card.component.html',
	styleUrls: ['./commons-card.component.less']
})
export class CommonsCardComponent extends CommonsComponent {

	constructor() {
		super();
	}

}
