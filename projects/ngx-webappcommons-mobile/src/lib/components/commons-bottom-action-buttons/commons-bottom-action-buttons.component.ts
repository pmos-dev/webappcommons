import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { EMaterialDesignIcons } from 'ngx-webappcommons-core';

import { ICommonsAction } from '../../interfaces/icommons-action';

@Component({
	selector: 'commons-bottom-action-buttons',
	templateUrl: './commons-bottom-action-buttons.component.html',
	styleUrls: ['./commons-bottom-action-buttons.component.less']
})
export class CommonsBottomActionButtonsComponent extends CommonsComponent {
	EMaterialDesignIcons = EMaterialDesignIcons;
	
	@Input() actions: ICommonsAction[] = [];
	
	@Output() onAction: EventEmitter<string> = new EventEmitter<string>();
	
	list(main: boolean): ICommonsAction[] {
		return this.actions
				.filter((a: ICommonsAction): boolean => a.main === main)
				.map((a: ICommonsAction): ICommonsAction => {
					a.iconSet = a.iconSet || EMaterialDesignIcons.MATERIALDESIGN;
					return a;
				});
	}
	
	doAction(action: string): void {
		this.onAction.emit(action);
	}
}
