import { Component, OnInit, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsUpService } from 'ngx-webappcommons-app';

import { CommonsDrawerService } from '../../services/commons-drawer.service';
import { CommonsToolbarStatusService } from '../../services/commons-toolbar-status.service';

@Component({
		selector: 'commons-toolbar',
		templateUrl: './commons-toolbar.component.html',
		styleUrls: ['./commons-toolbar.component.less']
})
export class CommonsToolbarComponent extends CommonsComponent implements OnInit {
	@Input() title: string|undefined;
	@Input() subtitle?: string;
	@Input() hasMenu: boolean = true;
		
	downTitle?: string;
	
	statusCount: number = 0;
	
	constructor(
			private upService: CommonsUpService,
			private drawerService: CommonsDrawerService,
			private toolbarStatusService: CommonsToolbarStatusService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();

		this.subscribe(
				this.toolbarStatusService.itemCountChangedObservable(),
				(count: number): void => {
					this.statusCount = count;
				}
		);

		this.subscribe(
				this.upService.titleObservable(),
				(title: string): void => { this.downTitle = title; }
		);
	}

	doToggleMenu(): void {
		this.drawerService.show('_drawermenu');
	}
	
	doUp(): void {
		this.upService.up();
	}
}
