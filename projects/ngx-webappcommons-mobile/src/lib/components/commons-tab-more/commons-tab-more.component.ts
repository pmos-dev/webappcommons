import { Component, OnInit, OnDestroy, Input, HostListener, HostBinding } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsMenuService } from 'ngx-webappcommons-app';

import { CommonsTabBarComponent } from '../commons-tab-bar/commons-tab-bar.component';

@Component({
		selector: 'commons-tab-more',
		templateUrl: './commons-tab-more.component.html',
		styleUrls: ['./commons-tab-more.component.less']
})
export class CommonsTabMoreComponent extends CommonsComponent implements OnInit, OnDestroy {
	@HostBinding('attr.disabled') @Input() disabled: boolean|undefined = undefined;

	@Input() menu!: string;
	
	show: boolean = false;
	
	tabBarCount: number = 0;

	@HostListener('click') doClick(): void {
		this.menuService.suppressBriefly(this.menu);
		this.show = true;
	}
	
	constructor(
			private menuService: CommonsMenuService,
			private tabbar: CommonsTabBarComponent
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.show = false;
		
		this.subscribe(
				this.menuService.hideObservable(),
				(name: string): void => {
					if (name !== this.menu) return;
					this.show = false;
				}
		);

		this.subscribe(
				this.tabbar.tabsChangedObservable(),
				(count: number): void => {
					this.tabBarCount = count;
				}
		);
		
		this.tabbar.addMore(this);
	}

	ngOnDestroy(): void {
		this.tabbar.removeMore();

		super.ngOnDestroy();
	}

}
