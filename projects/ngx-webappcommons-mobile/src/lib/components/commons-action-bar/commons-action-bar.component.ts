import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { ICommonsAction } from '../../interfaces/icommons-action';

@Component({
	selector: 'commons-action-bar',
	templateUrl: './commons-action-bar.component.html',
	styleUrls: ['./commons-action-bar.component.less']
})
export class CommonsActionBarComponent extends CommonsComponent {
	@Input() actions: ICommonsAction[] = [];
	
	@Output() onAction: EventEmitter<string> = new EventEmitter<string>();
	
	list(main: boolean): ICommonsAction[] {
		return this.actions
				.filter((a: ICommonsAction): boolean => a.main === main);
	}
	
	doAction(action: string): void {
		this.onAction.emit(action);
	}
}
