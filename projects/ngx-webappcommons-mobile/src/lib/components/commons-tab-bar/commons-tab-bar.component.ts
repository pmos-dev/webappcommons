import { Component, OnInit, Input, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsTabBarService } from '../../services/commons-tab-bar.service';

import { CommonsTabComponent } from '../commons-tab/commons-tab.component';
import { CommonsTabMoreComponent } from '../commons-tab-more/commons-tab-more.component';

@Component({
		selector: 'commons-tab-bar',
		templateUrl: './commons-tab-bar.component.html',
		styleUrls: ['./commons-tab-bar.component.less']
})
export class CommonsTabBarComponent extends CommonsComponent implements OnInit {
	@Input() name!: string;

	tabs: CommonsTabComponent[] = [];
	more: CommonsTabMoreComponent|undefined;

	show: boolean = true;
	
	tabCount: number = 0;
	private onTabsChanged: EventEmitter<number> = new EventEmitter<number>(true);
	
	constructor(
			private tabBarService: CommonsTabBarService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.show = true;

		this.subscribe(
				this.tabBarService.showObservable(),
				(name: string): void => {
					if (name === this.name) this.show = true;
				}
		);
		
		this.subscribe(
				this.tabBarService.hideObservable(),
				(name: string): void => {
					if (name === this.name) this.show = false;
				}
		);
	}

	public tabsChangedObservable(): Observable<number> {
		return this.onTabsChanged;
	}
	
	public addTab(tab: CommonsTabComponent): void {
		this.tabs.push(tab);
		this.tabCount++;
		
		this.onTabsChanged.emit(this.tabCount);
	}

	public removeTab(tab: CommonsTabComponent): void {
		const index: number = this.tabs.indexOf(tab);
		if (index === -1) return;
		
		this.tabs.slice(index, 1);
		if (this.tabCount > 0) this.tabCount--;
		
		this.onTabsChanged.emit(this.tabCount);
	}

	public addMore(more: CommonsTabMoreComponent): void {
		this.more = more;
		this.tabCount++;

		this.onTabsChanged.emit(this.tabCount);
	}

	public removeMore(): void {
		this.more = undefined;
		if (this.tabCount > 0) this.tabCount--;
	}

}
