import { Component, OnInit, OnDestroy, HostBinding, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsTabBarComponent } from '../commons-tab-bar/commons-tab-bar.component';

@Component({
	selector: '[commons-tab]',	// a tags so we can use routerLink
	templateUrl: './commons-tab.component.html',
	styleUrls: ['./commons-tab.component.less']
})
export class CommonsTabComponent extends CommonsComponent implements OnInit, OnDestroy {
	@HostBinding('attr.disabled') @Input() disabled: boolean|undefined = undefined;

	@Input() label: string|undefined;
	
	tabBarCount: number = 0;
	
	constructor(private tabbar: CommonsTabBarComponent) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();

		this.subscribe(
				this.tabbar.tabsChangedObservable(),
				(count: number): void => {
					this.tabBarCount = count;
				}
		);
		
		this.tabbar.addTab(this);
	}

	// NB for some reason Angular calls ngOnDestroy before ngOnInit prior to initialising the component??
	// So have to cope with this here.
	ngOnDestroy(): void {
		this.tabbar.removeTab(this);

		super.ngOnDestroy();
	}
}
