import { Component, Input, HostBinding, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: 'commons-radio',
		templateUrl: './commons-radio.component.html',
		styleUrls: ['./commons-radio.component.less']
})
export class CommonsRadioComponent extends CommonsComponent implements OnInit, OnChanges {
	@HostBinding('class.selected') isSelected!: boolean;
	@HostBinding('class.disabled') @Input() disabled: boolean = false;

	@Input() selected: boolean|string|number|undefined = false;
	@Output() selectedChange: EventEmitter<boolean|string|number> = new EventEmitter<boolean|string|number>();

	@Input() value: string|number|boolean|undefined = undefined;
	
	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.ngOnChanges();
	}

	ngOnChanges(): void {
		this.isSelected = this.value !== undefined && this.selected === this.value;
	}

	doClick(): void {
		if (this.disabled) return;
		
		this.selectedChange.emit(this.value);
	}
	
}
