import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter, ViewChild, HostBinding, ElementRef } from '@angular/core';

import { CommonsFinalValue } from 'tscommons-async';
import { TCommonsFinalValue } from 'tscommons-async';

import { CommonsComponent } from 'ngx-angularcommons-core';
import { CommonsFocusService } from 'ngx-angularcommons-browser';

import { CommonsMenuService } from 'ngx-webappcommons-app';

@Component({
	selector: 'commons-textbox',
	templateUrl: './commons-textbox.component.html',
	styleUrls: ['./commons-textbox.component.less']
})
export class CommonsTextboxComponent extends CommonsComponent implements OnInit, AfterViewInit {
	@HostBinding('class.forefront') forefront!: boolean;

	@ViewChild('textbox', { static: true }) private textbox: ElementRef|undefined;
	
	@Input() type: string = 'text';
	@Input() disabled: boolean = false;
	@Input() placeholder?: string;
	@Input() helper?: string;
	@Input() error?: string;
	@Input() options: string[] = [];
	@Input() optionsOnly: boolean = false;
	@Input() editable: boolean = true;
	@Input() clearable: boolean = true;
	@Input() customIcon?: string;
	@Input() autocompleteDelay: number = 500;
	@Input() autocompleteLength: number = 3;
	@Input() focusId?: string;

	@Output() valueChange: EventEmitter<string|undefined> = new EventEmitter<string|undefined>();
	@Input() value: string|undefined|null;

	@Output() onEdit: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onLostFocusAndChanged: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onEnterPressed: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onEscPressed: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onSelectChanged: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onCustomIcon: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onAutocomplete: EventEmitter<string> = new EventEmitter<string>(true);
	@Output() onCleared: EventEmitter<void> = new EventEmitter<void>(true);
	
	focused: boolean = false;
	isDropped: boolean = false;
	
	dropdownName: string;
	
	lastFocusValue: string|undefined|null;

	private autocomplete: CommonsFinalValue<string>|undefined;
	
	constructor(
			private menuService: CommonsMenuService,
			private focusService: CommonsFocusService
	) {
		super();
		
		this.dropdownName = `_dropdown_${Math.random()}`;
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.menuService.showObservable(),
				(name: string): void => {
					if (name === this.dropdownName) {
						this.forefront = true;
						this.isDropped = true;
					}
				}
		);
		this.subscribe(
				this.menuService.hideObservable(),
				(name: string): void => {
					if (name === this.dropdownName) {
						this.forefront = false;
						this.isDropped = false;
					}
				}
		);
		
		this.lastFocusValue = this.value;
		
		this.autocomplete = new CommonsFinalValue<string>(this.autocompleteDelay);
		
		this.subscribe(
				this.autocomplete.releaseObservable(),
				(data: TCommonsFinalValue<string>): void => {
					this.onAutocomplete.emit(data.value);
				}
		);
	}

	ngAfterViewInit(): void {
		this.subscribe(
				this.focusService.elementFocusObservable(),
				(id: string): void => {
					if (!this.focusId) return;
					if (this.focusId !== id) return;
					if (!this.textbox) return;
					
					this.textbox.nativeElement.focus();
				}
		);
	}

	getType(): string {
		if (this.type === undefined) return 'text';
		return this.type;
	}
	
	getValue(): string {
		if (this.value === undefined) return '';
		if (this.value === null) return '';
		return this.value;
	}
	
	doChanged(event: string|undefined): void {
		if (this.disabled) return;
		
		this.valueChange.emit(event);

		if (
				!this.value
				|| !this.autocomplete
				|| 'string' !== typeof this.value
				|| this.value.length < this.autocompleteLength
		) return;
		
		this.autocomplete.push(this.value);
	}
	
	doFocus(state: boolean): void {
		this.focused = state;

		if (this.focused) this.lastFocusValue = this.value;
		else {
			if (this.lastFocusValue !== this.value) this.onLostFocusAndChanged.emit();
		}
	}
	
	doDropdown(): void {
		if (this.disabled) return;
		
		if (this.textbox) this.textbox.nativeElement.focus();
		this.menuService.show(this.dropdownName);
	}
	
	doSelect(option: string): void {
		if (this.disabled) return;
		
		this.valueChange.emit(option);
		if (this.textbox) this.textbox.nativeElement.focus();
		
		this.onSelectChanged.emit();
	}
	
	doClick(): void {
		if (this.disabled) return;
		
		if (this.optionsOnly) this.doDropdown();
		if (this.editable) this.onEdit.emit();
	}
	
	doEnterPressed(): void {
		if (this.disabled) return;
		
		this.onEnterPressed.emit();
	}
	doEscPressed(): void {
		if (this.disabled) return;
		
		this.value = this.lastFocusValue;
		this.valueChange.emit(this.value);
		this.onEscPressed.emit();
	}
	
	doClear(): void {
		if (this.disabled || !this.clearable) return;
		this.valueChange.emit('');
		this.onCleared.emit();
	}
	
	doCustomIcon(): void {
		if (this.disabled || !this.customIcon) return;
		this.onCustomIcon.emit();
	}
}
