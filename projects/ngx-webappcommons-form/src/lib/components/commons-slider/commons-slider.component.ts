import { Component, OnInit, OnChanges } from '@angular/core';
import { Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { ECommonsSliderType } from '../../enums/ecommons-slider-type';

@Component({
	selector: 'commons-slider',
	templateUrl: './commons-slider.component.html',
	styleUrls: ['./commons-slider.component.less']
})
export class CommonsSliderComponent extends CommonsComponent implements OnInit, OnChanges {
	ECommonsSliderType = ECommonsSliderType;
	
	@ViewChild('barElement', { static: true }) private bar!: ElementRef;
	@ViewChild('draggerElement', { static: true }) private dragger!: ElementRef;
	@ViewChild('pressedElement', { static: true }) private pressed!: ElementRef;
	
	@Input() type!: ECommonsSliderType;
	@Input() label!: string;
	
	@Input() snapEnds: boolean = true;
	@Input() snapValues: boolean = true;
	@Input() decimalPlaces: number = 0;
	@Input() showValue: boolean = false;
	@Input() editValue: boolean = false;
	@Input() min?: number;
	@Input() max?: number;
	@Input() values?: string[];
	@Input() clickToSendSameValue: boolean = false;

	@Input() disabled: boolean = false;

	@Input() value?: string|number;
	@Output() valueChange: EventEmitter<string|number> = new EventEmitter<string|number>();

	active: boolean = false;
	invalid: boolean = false;
	focused: boolean = false;

	perc: number = 0;
	private panStartPerc: number = -1;
	
	constructor(
			private sanitized: DomSanitizer
	) {
		super();
	}
	
	ngOnInit(): void {
		super.ngOnInit();

		this.handleValueChange();
	}

	ngOnChanges(): void {
		this.handleValueChange();
	}

	private handleValueChange(): void {
		if (!this.value) {
			this.perc = 0;
			return;
		}
		
		switch (this.type) {
			case ECommonsSliderType.NUMERIC: {
				const min: number = this.min || 0;
				const max: number = this.max || 1;
				const delta: number = max - min;
				this.perc = (Number(this.value) - min) / delta;
				
				break;
			}
			case ECommonsSliderType.VALUES: {
				if (!this.values) return;
				const index: number = this.values.indexOf(String(this.value));
				this.perc = index / (this.values.length - 1);
				break;
			}
			case ECommonsSliderType.PERCENTAGE: {
				const min: number = this.min || 0;
				const max: number = this.max || 1;
				const delta: number = max - min;
				this.perc = (Number(this.value) - min) / delta;
				break;
			}
		}
	}

	getCSSPosition(): SafeStyle {
		let p: number = this.perc;
		
		this.invalid = false;
		if (p < 0) {
			p = 0;
			this.invalid = true;
		}
		if (p > 1) {
			p = 1;
			this.invalid = true;
		}
		
		return this.sanitized.bypassSecurityTrustStyle(`calc(${p} * 100%)`);
	}
	
	private generateValue(relative: number): any {
		switch (this.type) {
			case ECommonsSliderType.NUMERIC: {
				const min: number = this.min || 0;
				const max: number = this.max || 1;
				const delta: number = max - min;
				let v: number = min + (delta * relative);
				if (this.snapValues) {
					const scale = Math.pow(10, this.decimalPlaces);
					v = Math.round(v * scale) / scale;
				}
				
				return v;
			}
			case ECommonsSliderType.VALUES: {
				if (!this.values) return;
				const index: number = Math.round(relative * (this.values.length - 1));
				const v: string = this.values[index];

				return v;
			}
			case ECommonsSliderType.PERCENTAGE: {
				const min: number = this.min || 0;
				const max: number = this.max || 1;
				const delta: number = max - min;
				const v: number = min + (delta * relative);
				
				return Math.round(v * 100) / 100;
			}
		}
	}
	
	doJump(event: MouseEvent): void {
		// check we aren't clicking the dragger
		if (event.target === this.dragger.nativeElement) return;
		if (event.target === this.pressed.nativeElement) return;
		
		if (this.disabled) return;
		
		const width: number = this.bar.nativeElement.offsetWidth;
		let x: number = event.offsetX;
		
		if (x < 0) x = 0;
		if (x > width) x = width;
		
		let relative: number = x / width;
		
		if (this.snapEnds) {
			if (relative < 0.05) relative = 0;
			if (relative > 0.95) relative = 1;
		}
		
		this.valueChange.emit(this.generateValue(relative));
		
		this.active = true;
		this.setTimeout(200, async (): Promise<void> => { this.active = false; });
	}
	
	doPanStart(event: HammerInput): void {
		if (this.disabled) return;
		
		this.panStartPerc = this.perc;
		
		this.active = true;
		
		this.handlePan(event);
	}
	
	private handlePan(event: HammerInput): void {
		if (this.panStartPerc === -1) return;
		
		const delta: number = event.deltaX / this.bar.nativeElement.offsetWidth;
		
		let perc: number = this.panStartPerc + delta;
		if (perc < 0) perc = 0;
		if (perc > 1) perc = 1;
		
		this.valueChange.emit(this.generateValue(perc));
	}
	
	doPanMove(event: HammerInput): void {
		if (this.disabled) return;
		if (event.direction !== 2 && event.direction !== 4) return;
		
		this.handlePan(event);
	}
	
	doPanEnd(event: HammerInput): void {
		if (this.disabled) return;
		
		this.handlePan(event);

		this.panStartPerc = -1;
		this.active = false;
	}
				
	doFocus(state: boolean): void {
		this.focused = state;
	}
	
	doEdit(): void {
		this.valueChange.emit(this.value);
	}
	
	doClick(): void {
		if (this.clickToSendSameValue) this.valueChange.emit(this.value);
	}
}
