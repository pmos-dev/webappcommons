import { Component, Input, HostBinding, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: 'commons-checkbox',
		templateUrl: './commons-checkbox.component.html',
		styleUrls: ['./commons-checkbox.component.less']
})
export class CommonsCheckboxComponent extends CommonsComponent {
	@HostBinding('class.checked') @Input() checked: boolean = false;
	@Output() checkedChange: EventEmitter<boolean> = new EventEmitter<boolean>();

	@HostBinding('class.disabled') @Input() disabled: boolean = false;

	constructor() {
		super();
	}

	doClick(): void {
		if (this.disabled) return;
		
		this.checkedChange.emit(!this.checked);
	}
	
}
