import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsCoreModule } from 'ngx-angularcommons-core';
import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';
import { NgxAngularCommonsBrowserModule } from 'ngx-angularcommons-browser';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';

import { CommonsCheckboxComponent } from './components/commons-checkbox/commons-checkbox.component';
import { CommonsTextboxComponent } from './components/commons-textbox/commons-textbox.component';
import { CommonsSliderComponent } from './components/commons-slider/commons-slider.component';
import { CommonsTextareaComponent } from './components/commons-textarea/commons-textarea.component';
import { CommonsRadioComponent } from './components/commons-radio/commons-radio.component';

@NgModule({
		imports: [
				CommonModule,
				FormsModule,
				NgxAngularCommonsCoreModule,
				NgxAngularCommonsPipeModule,
				NgxAngularCommonsBrowserModule,
				NgxWebAppCommonsCoreModule,
				NgxWebAppCommonsAppModule
		],
		declarations: [
				CommonsTextboxComponent,
				CommonsCheckboxComponent,
				CommonsSliderComponent,
				CommonsTextareaComponent,
				CommonsRadioComponent
		],
		exports: [
				CommonsTextboxComponent,
				CommonsCheckboxComponent,
				CommonsSliderComponent,
				CommonsTextareaComponent,
				CommonsRadioComponent
		]
})
export class NgxWebAppCommonsFormModule {
	static forRoot(): ModuleWithProviders<NgxWebAppCommonsFormModule> {
		return {
				ngModule: NgxWebAppCommonsFormModule,
				providers: [
				]
		};
	}
}
