import { CommonsType } from 'tscommons-core';

export enum ECommonsTable2Selectable {
		SINGLE = 'single',
		MULTIPLE = 'multiple'
}

export function toECommonsTable2Selectable(type: string): ECommonsTable2Selectable|undefined {
	switch (type) {
		case ECommonsTable2Selectable.SINGLE.toString():
			return ECommonsTable2Selectable.SINGLE;
		case ECommonsTable2Selectable.MULTIPLE.toString():
			return ECommonsTable2Selectable.MULTIPLE;
	}
	return undefined;
}

export function fromECommonsTable2Selectable(type: ECommonsTable2Selectable): string {
	switch (type) {
		case ECommonsTable2Selectable.SINGLE:
			return ECommonsTable2Selectable.SINGLE.toString();
		case ECommonsTable2Selectable.MULTIPLE:
			return ECommonsTable2Selectable.MULTIPLE.toString();
	}
	
	throw new Error('Unknown ECommonsTable2Selectable');
}

export function isECommonsTable2Selectable(test: unknown): test is ECommonsTable2Selectable {
	if (!CommonsType.isString(test)) return false;
	
	return toECommonsTable2Selectable(test) !== undefined;
}

export function keyToECommonsTable2Selectable(key: string): ECommonsTable2Selectable {
	switch (key) {
		case 'SINGLE':
			return ECommonsTable2Selectable.SINGLE;
		case 'MULTIPLE':
			return ECommonsTable2Selectable.MULTIPLE;
	}
	
	throw new Error(`Unable to obtain ECommonsTable2Selectable for key: ${key}`);
}

export const ECOMMONS_TABLE2_SELECTABLES: ECommonsTable2Selectable[] = Object.keys(ECommonsTable2Selectable)
		.map((e: string): ECommonsTable2Selectable => keyToECommonsTable2Selectable(e));
