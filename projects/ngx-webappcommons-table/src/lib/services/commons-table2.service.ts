import { Injectable, EventEmitter } from '@angular/core';

import { Observable, ReplaySubject } from 'rxjs';

import { CommonsTable2Component } from '../components/commons-table2/commons-table2.component';

import { TCommonsTable2Column } from '../types/tcommons-table2-column';

import { ECommonsTable2Selectable } from '../enums/ecommons-table2-selectable';

@Injectable()
export class CommonsTable2Service {
	private onColumnsChanged: Map<
			CommonsTable2Component,
			ReplaySubject<TCommonsTable2Column[]>
	> = new Map<
			CommonsTable2Component,
			ReplaySubject<TCommonsTable2Column[]>
	>();

	private onSelectable: Map<
			CommonsTable2Component,
			ReplaySubject<ECommonsTable2Selectable|undefined>
	> = new Map<
			CommonsTable2Component,
			ReplaySubject<ECommonsTable2Selectable|undefined>
	>();
	private selectable: Map<
			CommonsTable2Component,
			ECommonsTable2Selectable|undefined
	> = new Map<
			CommonsTable2Component,
			ECommonsTable2Selectable
	>();

	private onDisabled: Map<
			CommonsTable2Component,
			ReplaySubject<boolean>
	> = new Map<
			CommonsTable2Component,
			ReplaySubject<boolean>
	>();
	private disabled: Map<
			CommonsTable2Component,
			boolean
	> = new Map<
			CommonsTable2Component,
			boolean
	>();
	
	public registerTable(component: CommonsTable2Component): void {
		this.onColumnsChanged.set(component, new ReplaySubject<TCommonsTable2Column[]>(1));

		this.onSelectable.set(component, new ReplaySubject<ECommonsTable2Selectable|undefined>(1));
		this.selectable.set(component, undefined);

		this.onDisabled.set(component, new ReplaySubject<boolean>(1));
		this.disabled.set(component, false);
	}
	
	public columnsChangedObservable(component: CommonsTable2Component): Observable<TCommonsTable2Column[]> {
		if (!this.onColumnsChanged.has(component)) throw new Error('Unregistered table');
		
		return this.onColumnsChanged.get(component)!;
	}
	
	public cascadeColumnsChanged(
			component: CommonsTable2Component,
			columns: TCommonsTable2Column[]
	): void {
		if (!this.onColumnsChanged.has(component)) throw new Error('Unregistered table');
		
		this.onColumnsChanged.get(component)!.next(columns);
	}
	
	public selectableObservable(component: CommonsTable2Component): Observable<ECommonsTable2Selectable|undefined> {
		if (!this.onSelectable.has(component)) throw new Error('Unregistered table');
		
		return this.onSelectable.get(component)!;
	}
	
	public cascadeSelectable(
			component: CommonsTable2Component,
			selectable: ECommonsTable2Selectable|undefined
	): void {
		if (!this.onSelectable.has(component)) throw new Error('Unregistered table');
		
		this.selectable.set(component, selectable);
		this.onSelectable.get(component)!.next(selectable);
	}
	
	public getSelectable(
			component: CommonsTable2Component
	): ECommonsTable2Selectable|undefined {
		if (!this.selectable.has(component)) throw new Error('Unregistered table');
		
		return this.selectable.get(component)!;
	}
	
	public disabledObservable(component: CommonsTable2Component): Observable<boolean> {
		if (!this.onSelectable.has(component)) throw new Error('Unregistered table');
		
		return this.onDisabled.get(component)!;
	}
	
	public cascadeDisabled(
			component: CommonsTable2Component,
			disabled: boolean
	): void {
		if (!this.onDisabled.has(component)) throw new Error('Unregistered table');
		
		this.disabled.set(component, disabled);
		this.onDisabled.get(component)!.next(disabled);
	}
	
	public getDisabled(
			component: CommonsTable2Component
	): boolean {
		if (!this.disabled.has(component)) throw new Error('Unregistered table');
		
		return this.disabled.get(component)!;
	}
}
