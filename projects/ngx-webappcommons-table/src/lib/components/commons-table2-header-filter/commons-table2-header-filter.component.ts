import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: '[commons-table2-header-filter]',
		templateUrl: './commons-table2-header-filter.component.html',
		styleUrls: ['./commons-table2-header-filter.component.less']
})
export class CommonsTable2HeaderFilterComponent extends CommonsComponent {}
