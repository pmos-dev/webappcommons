import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: '[commons-table2-body]',
		templateUrl: './commons-table2-body.component.html',
		styleUrls: ['./commons-table2-body.component.less']
})
export class CommonsTable2BodyComponent extends CommonsComponent {}
