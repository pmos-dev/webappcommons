import { Component, OnInit, Input, Output, EventEmitter, HostBinding } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsTable2Service } from '../../services/commons-table2.service';

import { CommonsTable2Columns } from '../../helpers/commons-table2-columns';

import { TCommonsTable2Column } from '../../types/tcommons-table2-column';

import { ECommonsTable2Selectable } from '../../enums/ecommons-table2-selectable';

import { CommonsTable2Component } from '../commons-table2/commons-table2.component';

@Component({
		selector: '[commons-table2-body-row]',
		templateUrl: './commons-table2-body-row.component.html',
		styleUrls: ['./commons-table2-body-row.component.less']
})
export class CommonsTable2BodyRowComponent extends CommonsComponent implements OnInit {
	@HostBinding('style.gridTemplateColumns') hostGridTemplateColumns: string|undefined;
	@HostBinding('class.selected') @Input() selected?: boolean;
	@Output() selectedChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);

	selectable: ECommonsTable2Selectable|undefined;
	disabled: boolean = false;

	constructor(
			private root: CommonsTable2Component,
			private service: CommonsTable2Service
	) {
		super();
	}
	
	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.service.selectableObservable(this.root),
				(selectable: ECommonsTable2Selectable|undefined): void => {
					setTimeout(
							(): void => { this.selectable = selectable; },
							0
					);
				}
		);
		this.selectable = this.service.getSelectable(this.root);
		
		this.subscribe(
				this.service.disabledObservable(this.root),
				(disabled: boolean): void => {
					setTimeout(
							(): void => { this.disabled = disabled; },
							0
					);
				}
		);
		this.disabled = this.service.getDisabled(this.root);

		this.subscribe(
				this.service.columnsChangedObservable(this.root),
				(columns: TCommonsTable2Column[]): void => {
					let widths: string = CommonsTable2Columns.getGridTemplateColumns(columns);
					if (this.selectable !== undefined) widths = `52px ${widths}`;

					setTimeout(
							(): void => { this.hostGridTemplateColumns = widths; },
							0
					);
				}
		);
	}
	
	doSelected(): void {
		this.selectedChange.emit(this.selected);
	}
}
