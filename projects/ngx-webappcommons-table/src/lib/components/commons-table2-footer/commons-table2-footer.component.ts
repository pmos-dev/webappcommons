import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: '[commons-table2-footer]',
		templateUrl: './commons-table2-footer.component.html',
		styleUrls: ['./commons-table2-footer.component.less']
})
export class CommonsTable2FooterComponent extends CommonsComponent {}
