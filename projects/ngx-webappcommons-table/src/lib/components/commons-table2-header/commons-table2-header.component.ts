import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: '[commons-table2-header]',
		templateUrl: './commons-table2-header.component.html',
		styleUrls: ['./commons-table2-header.component.less']
})
export class CommonsTable2HeaderComponent extends CommonsComponent {}
