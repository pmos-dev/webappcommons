import { Component, OnInit, AfterContentInit, OnChanges, SimpleChanges, Input } from '@angular/core';
import { ContentChildren, QueryList } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsTable2Service } from '../../services/commons-table2.service';

import { CommonsTable2Columns } from '../../helpers/commons-table2-columns';

import { TCommonsTable2Column } from '../../types/tcommons-table2-column';

import { ECommonsTable2Selectable } from '../../enums/ecommons-table2-selectable';

import { CommonsTable2HeaderColumnComponent } from '../commons-table2-header-column/commons-table2-header-column.component';
import { CommonsTable2BodyComponent } from '../commons-table2-body/commons-table2-body.component';

@Component({
		selector: '[commons-table2]',
		templateUrl: './commons-table2.component.html',
		styleUrls: ['./commons-table2.component.less']
})
export class CommonsTable2Component extends CommonsComponent implements OnInit, AfterContentInit, OnChanges {
	@ContentChildren(CommonsTable2HeaderColumnComponent, { descendants: true }) columnsContentChildren: QueryList<CommonsTable2HeaderColumnComponent>|undefined;
	@ContentChildren(CommonsTable2BodyComponent) bodyContentChildren: QueryList<CommonsTable2BodyComponent>|undefined;

	@Input() selectable: ECommonsTable2Selectable|undefined;
	@Input() disabled: boolean = false;

	constructor(
			private service: CommonsTable2Service
	) {
		super();
		
		this.service.registerTable(this);
	}

	// these have to be public so that the other component classes can access them
	public columns: TCommonsTable2Column[] = [];
	
	ngOnInit(): void {
		super.ngOnInit();
		
		this.service.cascadeSelectable(this, this.selectable);
		this.service.cascadeDisabled(this, this.disabled);
	}
	
	ngOnChanges(changes: SimpleChanges): void {
		if (changes.selectable && changes.selectable.currentValue !== changes.selectable.previousValue) {
			this.service.cascadeSelectable(this, this.selectable);
		}
		if (changes.disabled && changes.disabled.currentValue !== changes.disabled.previousValue) {
			this.service.cascadeDisabled(this, this.disabled);
		}
	}
	
	ngAfterContentInit(): void {
		this.subscribe(
				this.columnsContentChildren.changes,
				(): void => {
					this.refreshColumns();
				}
		);
		this.refreshColumns();
	}
	
	private refreshColumns(): void {
		this.columns = CommonsTable2Columns.listColumns(this.columnsContentChildren);
		
		this.service.cascadeColumnsChanged(this, this.columns);
	}
}
