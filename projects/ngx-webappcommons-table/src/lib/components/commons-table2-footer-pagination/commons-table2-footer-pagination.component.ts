import { Component, OnChanges, SimpleChanges, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: '[commons-table2-footer-pagination]',
		templateUrl: './commons-table2-footer-pagination.component.html',
		styleUrls: ['./commons-table2-footer-pagination.component.less']
})
export class CommonsTable2FooterPaginationComponent extends CommonsComponent implements OnChanges {
	@Input() from: number|undefined;
	@Input() total: number|undefined;
	
	@Input() rows: number|undefined;
	@Output() private rowsChange: EventEmitter<number> = new EventEmitter<number>(true);

	@Output() onFirst: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onBack: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onForward: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onLast: EventEmitter<void> = new EventEmitter<void>(true);
	
	internalRows: string|undefined;
	canBack: boolean|undefined;
	canForward: boolean|undefined;
	
	doChangeRows(): void {
		this.rowsChange.emit(parseInt(this.internalRows, 10));
	}
	
	ngOnChanges(_changes: SimpleChanges): void {
		this.internalRows = this.rows === undefined ? undefined : this.rows.toString();
		
		const thisPageEnd: number = (this.from || 0) + (this.rows || 0);

		this.canBack = this.rows !== undefined
				&& this.from !== undefined
				&& this.total
				&& this.from > 0;
		this.canForward = this.rows !== undefined
				&& this.total
				&& thisPageEnd < this.total;
	}
}
