import { Component, OnInit, AfterContentInit, HostBinding, Input, Output, EventEmitter } from '@angular/core';
import { ContentChildren, QueryList } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsTable2Service } from '../../services/commons-table2.service';

import { CommonsTable2Columns } from '../../helpers/commons-table2-columns';

import { TCommonsTable2Column } from '../../types/tcommons-table2-column';

import { ECommonsTable2Selectable } from '../../enums/ecommons-table2-selectable';

import { CommonsTable2HeaderColumnComponent } from '../commons-table2-header-column/commons-table2-header-column.component';
import { CommonsTable2Component } from '../commons-table2/commons-table2.component';

@Component({
		selector: '[commons-table2-header-columns]',
		templateUrl: './commons-table2-header-columns.component.html',
		styleUrls: ['./commons-table2-header-columns.component.less']
})
export class CommonsTable2HeaderColumnsComponent extends CommonsComponent implements OnInit, AfterContentInit {
	ECommonsTable2Selectable = ECommonsTable2Selectable;
	
	@HostBinding('style.gridTemplateColumns') hostGridTemplateColumns: string|undefined;
	@ContentChildren(CommonsTable2HeaderColumnComponent) contentChildren: QueryList<CommonsTable2HeaderColumnComponent>|undefined;
	
	@Input() selectAll: boolean = false;
	@Output() selectAllChange: EventEmitter<boolean> = new EventEmitter<boolean>(true);
	
	selectable: ECommonsTable2Selectable|undefined;
	disabled: boolean = false;
	
	constructor(
			private root: CommonsTable2Component,
			private service: CommonsTable2Service
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.service.selectableObservable(this.root),
				(selectable: ECommonsTable2Selectable|undefined): void => {
					setTimeout(
							(): void => { this.selectable = selectable; },
							0
					);
				}
		);
		this.selectable = this.service.getSelectable(this.root);
		
		this.subscribe(
				this.service.disabledObservable(this.root),
				(disabled: boolean): void => {
					setTimeout(
							(): void => { this.disabled = disabled; },
							0
					);
				}
		);
		this.disabled = this.service.getDisabled(this.root);
	}
	
	ngAfterContentInit(): void {
		this.subscribe(
				this.contentChildren.changes,
				(): void => {
					this.refreshColumns();
				}
		);
		this.refreshColumns();
	}
	
	private refreshColumns(): void {
		const columns: TCommonsTable2Column[] = CommonsTable2Columns.listColumns(this.contentChildren);
		
		let widths: string = CommonsTable2Columns.getGridTemplateColumns(columns);
		if (this.selectable) widths = `52px ${widths}`;
		
		setTimeout(
				(): void => { this.hostGridTemplateColumns = widths; },
				0
		);
	}
	
	doSelectAll(): void {
		this.selectAllChange.emit(this.selectAll);
	}
}
