import { Component, AfterContentInit, Input, HostBinding, ElementRef } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: '[commons-table2-header-column]',
		templateUrl: './commons-table2-header-column.component.html',
		styleUrls: ['./commons-table2-header-column.component.less']
})
export class CommonsTable2HeaderColumnComponent extends CommonsComponent implements AfterContentInit {
	@HostBinding('title') @Input() public title: string|undefined;

	@Input() public name: string|undefined;
	@Input() public width?: string;
	
	constructor(
			private ref: ElementRef
	) {
		super();
	}
	
	ngAfterContentInit(): void {
		if (!this.title) this.title = this.ref.nativeElement.innerText;
	}
}
