import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: '[commons-table2-body-cell]',
		templateUrl: './commons-table2-body-cell.component.html',
		styleUrls: ['./commons-table2-body-cell.component.less']
})
export class CommonsTable2BodyCellComponent extends CommonsComponent {}
