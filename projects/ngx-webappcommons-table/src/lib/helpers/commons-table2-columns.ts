import { QueryList } from '@angular/core';

import { CommonsTable2HeaderColumnComponent } from '../components/commons-table2-header-column/commons-table2-header-column.component';

import { TCommonsTable2Column } from '../types/tcommons-table2-column';

export class CommonsTable2Columns {
	public static listColumns(
			contentChildren?: QueryList<CommonsTable2HeaderColumnComponent>
	): TCommonsTable2Column[] {
		if (!contentChildren) return [];
		
		const columns: TCommonsTable2Column[] = contentChildren
				.map((child: CommonsTable2HeaderColumnComponent): TCommonsTable2Column => ({
						name: child.name,
						width: child.width
				}));
				
		return columns;
	}
	
	public static getGridTemplateColumns(
			columns: TCommonsTable2Column[]
	): string {
		const widths: string[] = columns
				.map((column: TCommonsTable2Column): string => column.width || '1fr');
		
		return widths.join(' ');
	}
}
