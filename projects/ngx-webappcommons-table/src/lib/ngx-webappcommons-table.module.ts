import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsCoreModule } from 'ngx-angularcommons-core';
import { NgxAngularCommonsBrowserModule } from 'ngx-angularcommons-browser';
import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsFormModule } from 'ngx-webappcommons-form';

import { CommonsTable2Component } from './components/commons-table2/commons-table2.component';
import { CommonsTable2HeaderComponent } from './components/commons-table2-header/commons-table2-header.component';
import { CommonsTable2HeaderFilterComponent } from './components/commons-table2-header-filter/commons-table2-header-filter.component';
import { CommonsTable2HeaderColumnsComponent } from './components/commons-table2-header-columns/commons-table2-header-columns.component';
import { CommonsTable2HeaderColumnComponent } from './components/commons-table2-header-column/commons-table2-header-column.component';
import { CommonsTable2BodyComponent } from './components/commons-table2-body/commons-table2-body.component';
import { CommonsTable2BodyRowComponent } from './components/commons-table2-body-row/commons-table2-body-row.component';
import { CommonsTable2BodyCellComponent } from './components/commons-table2-body-cell/commons-table2-body-cell.component';
import { CommonsTable2FooterComponent } from './components/commons-table2-footer/commons-table2-footer.component';
import { CommonsTable2FooterPaginationComponent } from './components/commons-table2-footer-pagination/commons-table2-footer-pagination.component';

import { CommonsTable2Service } from './services/commons-table2.service';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsCoreModule,
				NgxAngularCommonsBrowserModule,
				NgxAngularCommonsPipeModule,
				NgxWebAppCommonsCoreModule,
				NgxWebAppCommonsFormModule
		],
		declarations: [
				CommonsTable2Component,
				CommonsTable2HeaderComponent,
				CommonsTable2HeaderFilterComponent,
				CommonsTable2HeaderColumnsComponent,
				CommonsTable2HeaderColumnComponent,
				CommonsTable2BodyComponent,
				CommonsTable2BodyRowComponent,
				CommonsTable2BodyCellComponent,
				CommonsTable2FooterComponent,
				CommonsTable2FooterPaginationComponent
		],
		exports: [
				CommonsTable2Component,
				CommonsTable2HeaderComponent,
				CommonsTable2HeaderFilterComponent,
				CommonsTable2HeaderColumnsComponent,
				CommonsTable2HeaderColumnComponent,
				CommonsTable2BodyComponent,
				CommonsTable2BodyRowComponent,
				CommonsTable2BodyCellComponent,
				CommonsTable2FooterComponent,
				CommonsTable2FooterPaginationComponent
		]
})
export class NgxWebAppCommonsTableModule {
	static forRoot(): ModuleWithProviders<NgxWebAppCommonsTableModule> {
		return {
				ngModule: NgxWebAppCommonsTableModule,
				providers: [
						CommonsTable2Service
				]
		};
	}
}
