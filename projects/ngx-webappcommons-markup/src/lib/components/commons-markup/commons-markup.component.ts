import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-markup',
	templateUrl: './commons-markup.component.html',
	styleUrls: ['./commons-markup.component.less']
})
export class CommonsMarkupComponent extends CommonsComponent {
	@Input() markup!: string;
}
