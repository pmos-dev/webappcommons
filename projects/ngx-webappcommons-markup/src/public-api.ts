/*
 * Public API Surface of ngx-webappcommons-markup
 */

export * from './lib/components/commons-markup-inner/commons-markup-inner.component';
export * from './lib/components/commons-markup/commons-markup.component';

export * from './lib/ngx-webappcommons-markup.module';
