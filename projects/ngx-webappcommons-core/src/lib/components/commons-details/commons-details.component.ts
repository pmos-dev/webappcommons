import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: '[commons-details]',
	templateUrl: './commons-details.component.html',
	styleUrls: ['./commons-details.component.less']
})
export class CommonsDetailsComponent extends CommonsComponent {
}
