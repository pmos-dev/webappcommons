import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-body-background',
	templateUrl: './commons-body-background.component.html',
	styleUrls: ['./commons-body-background.component.less']
})
export class CommonsBodyBackgroundComponent extends CommonsComponent {

	constructor() {
		super();
	}

}
