import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-info-line',
	templateUrl: './commons-info-line.component.html',
	styleUrls: ['./commons-info-line.component.less']
})
export class CommonsInfoLineComponent extends CommonsComponent {
	@Input() allowClose!: boolean;
	
	@Output() onClose: EventEmitter<void> = new EventEmitter<void>();

	constructor() {
		super();
	}

}
