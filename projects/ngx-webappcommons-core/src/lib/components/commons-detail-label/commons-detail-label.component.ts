import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: '[commons-detail-label]',
	templateUrl: './commons-detail-label.component.html',
	styleUrls: ['./commons-detail-label.component.less']
})
export class CommonsDetailLabelComponent extends CommonsComponent {
	@Input() icon!: string;

}
