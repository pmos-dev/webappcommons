import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-button-bar',
	templateUrl: './commons-button-bar.component.html',
	styleUrls: ['./commons-button-bar.component.less']
})
export class CommonsButtonBarComponent extends CommonsComponent {
	constructor() {
		super();
	}

}
