import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: '[commons-detail-text]',
	templateUrl: './commons-detail-text.component.html',
	styleUrls: ['./commons-detail-text.component.less']
})
export class CommonsDetailTextComponent extends CommonsComponent {
	@Input() editable: boolean = false;
	
	@Output() onEdit: EventEmitter<void> = new EventEmitter<void>();

	doEdit(): void {
		this.onEdit.emit();
	}
}
