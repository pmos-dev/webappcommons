import { CommonsType } from 'tscommons-core';

import { ECommonsThemeColor, isECommonsThemeColor } from '../enums/ecommons-theme-color';
import { ECommonsThemeContrast, isECommonsThemeContrast } from '../enums/ecommons-theme-contrast';

export type TCommonsThemeConfig = {
		color: ECommonsThemeColor;
		contrast: ECommonsThemeContrast;
};

export function isTCommonsThemeConfig(test: unknown): test is TCommonsThemeConfig {
	if (!CommonsType.hasPropertyEnum<ECommonsThemeColor>(test, 'color', isECommonsThemeColor)) return false;
	if (!CommonsType.hasPropertyEnum<ECommonsThemeContrast>(test, 'contrast', isECommonsThemeContrast)) return false;

	return true;
}
