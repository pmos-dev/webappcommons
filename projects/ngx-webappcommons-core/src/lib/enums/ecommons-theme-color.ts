import { CommonsType } from 'tscommons-core';

export enum ECommonsThemeColor {
		BLUE = 'blue',
		PURPLE = 'purple',
		DARKRED = 'darkred',
		MUTED = 'muted',
		NIGHT = 'night',
		MARINE = 'marine',
		HOTPINK = 'hotpink'
}

export function toECommonsThemeColor(type: string): ECommonsThemeColor|undefined {
	switch (type) {
		case ECommonsThemeColor.BLUE.toString():
			return ECommonsThemeColor.BLUE;
		case ECommonsThemeColor.PURPLE.toString():
			return ECommonsThemeColor.PURPLE;
		case ECommonsThemeColor.DARKRED.toString():
			return ECommonsThemeColor.DARKRED;
		case ECommonsThemeColor.MUTED.toString():
			return ECommonsThemeColor.MUTED;
		case ECommonsThemeColor.NIGHT.toString():
			return ECommonsThemeColor.NIGHT;
		case ECommonsThemeColor.MARINE.toString():
			return ECommonsThemeColor.MARINE;
		case ECommonsThemeColor.HOTPINK.toString():
			return ECommonsThemeColor.HOTPINK;
	}
	return undefined;
}

export function fromECommonsThemeColor(type: ECommonsThemeColor): string {
	switch (type) {
		case ECommonsThemeColor.BLUE:
			return ECommonsThemeColor.BLUE.toString();
		case ECommonsThemeColor.PURPLE:
			return ECommonsThemeColor.PURPLE.toString();
		case ECommonsThemeColor.DARKRED:
			return ECommonsThemeColor.DARKRED.toString();
		case ECommonsThemeColor.MUTED:
			return ECommonsThemeColor.MUTED.toString();
		case ECommonsThemeColor.NIGHT:
			return ECommonsThemeColor.NIGHT.toString();
		case ECommonsThemeColor.MARINE:
			return ECommonsThemeColor.MARINE.toString();
		case ECommonsThemeColor.HOTPINK:
			return ECommonsThemeColor.HOTPINK.toString();
	}
	
	throw new Error('Unknown ECommonsThemeColor');
}

export function isECommonsThemeColor(test: unknown): test is ECommonsThemeColor {
	if (!CommonsType.isString(test)) return false;
	
	return toECommonsThemeColor(test) !== undefined;
}

export function keyToECommonsThemeColor(key: string): ECommonsThemeColor {
	switch (key) {
		case 'BLUE':
			return ECommonsThemeColor.BLUE;
		case 'PURPLE':
			return ECommonsThemeColor.PURPLE;
		case 'DARKRED':
			return ECommonsThemeColor.DARKRED;
		case 'MUTED':
			return ECommonsThemeColor.MUTED;
		case 'NIGHT':
			return ECommonsThemeColor.NIGHT;
		case 'MARINE':
			return ECommonsThemeColor.MARINE;
		case 'HOTPINK':
			return ECommonsThemeColor.HOTPINK;
	}
	
	throw new Error(`Unable to obtain ECommonsThemeColor for key: ${key}`);
}

export const ECOMMONS_THEME_COLORS: ECommonsThemeColor[] = Object.keys(ECommonsThemeColor)
		.map((e: string): ECommonsThemeColor => keyToECommonsThemeColor(e));
