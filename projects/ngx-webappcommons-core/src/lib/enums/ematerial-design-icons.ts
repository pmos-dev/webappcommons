import { CommonsType } from 'tscommons-core';

export enum EMaterialDesignIcons {
		MATERIALDESIGN = 'material-design',
		MATERIALDESIGN3 = 'material-design-3',
		MDI = 'mdi'
}

export function toEMaterialDesignIcons(type: string): EMaterialDesignIcons|undefined {
	switch (type) {
		case EMaterialDesignIcons.MATERIALDESIGN.toString():
			return EMaterialDesignIcons.MATERIALDESIGN;
		case EMaterialDesignIcons.MATERIALDESIGN3.toString():
			return EMaterialDesignIcons.MATERIALDESIGN3;
		case EMaterialDesignIcons.MDI.toString():
			return EMaterialDesignIcons.MDI;
	}
	return undefined;
}

export function fromEMaterialDesignIcons(type: EMaterialDesignIcons): string {
	switch (type) {
		case EMaterialDesignIcons.MATERIALDESIGN:
			return EMaterialDesignIcons.MATERIALDESIGN.toString();
		case EMaterialDesignIcons.MATERIALDESIGN3:
			return EMaterialDesignIcons.MATERIALDESIGN3.toString();
		case EMaterialDesignIcons.MDI:
			return EMaterialDesignIcons.MDI.toString();
	}
	
	throw new Error('Unknown EMaterialDesignIcons');
}

export function isEMaterialDesignIcons(test: unknown): test is EMaterialDesignIcons {
	if (!CommonsType.isString(test)) return false;
	
	return toEMaterialDesignIcons(test) !== undefined;
}

export function keyToEMaterialDesignIcons(key: string): EMaterialDesignIcons {
	switch (key) {
		case 'MATERIALDESIGN':
			return EMaterialDesignIcons.MATERIALDESIGN;
		case 'MATERIALDESIGN3':
			return EMaterialDesignIcons.MATERIALDESIGN3;
		case 'MDI':
			return EMaterialDesignIcons.MDI;
	}
	
	throw new Error(`Unable to obtain EMaterialDesignIcons for key: ${key}`);
}

export const EMATERIAL_DESIGN_ICONSS: EMaterialDesignIcons[] = Object.keys(EMaterialDesignIcons)
		.map((e: string): EMaterialDesignIcons => keyToEMaterialDesignIcons(e));
