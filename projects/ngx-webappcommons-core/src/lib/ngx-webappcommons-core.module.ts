import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsCoreModule } from 'ngx-angularcommons-core';
import { NgxAngularCommonsAppModule } from 'ngx-angularcommons-app';

import { CommonsPaneComponent } from './components/commons-pane/commons-pane.component';
import { CommonsButtonComponent } from './components/commons-button/commons-button.component';
import { CommonsButtonBarComponent } from './components/commons-button-bar/commons-button-bar.component';
import { CommonsInfoLineComponent } from './components/commons-info-line/commons-info-line.component';
import { CommonsDetailsComponent } from './components/commons-details/commons-details.component';
import { CommonsDetailLabelComponent } from './components/commons-detail-label/commons-detail-label.component';
import { CommonsDetailTextComponent } from './components/commons-detail-text/commons-detail-text.component';
import { CommonsBodyBackgroundComponent } from './components/commons-body-background/commons-body-background.component';

import { CommonsThemeService } from './services/commons-theme.service';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsAppModule,
				NgxAngularCommonsCoreModule
		],
		declarations: [
				CommonsPaneComponent,
				CommonsInfoLineComponent,
				CommonsButtonComponent,
				CommonsButtonBarComponent,
				CommonsDetailsComponent,
				CommonsDetailLabelComponent,
				CommonsDetailTextComponent,
				CommonsBodyBackgroundComponent
		],
		exports: [
				CommonsPaneComponent,
				CommonsInfoLineComponent,
				CommonsButtonComponent,
				CommonsButtonBarComponent,
				CommonsDetailsComponent,
				CommonsDetailLabelComponent,
				CommonsDetailTextComponent,
				CommonsBodyBackgroundComponent
		]
})
export class NgxWebAppCommonsCoreModule {
	static forRoot(): ModuleWithProviders<NgxWebAppCommonsCoreModule> {
		return {
				ngModule: NgxWebAppCommonsCoreModule,
				providers: [
						CommonsThemeService
				]
		};
	}
}
