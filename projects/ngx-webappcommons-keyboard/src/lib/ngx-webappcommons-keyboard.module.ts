import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';

import { CommonsKeyboardKeyComponent } from './components/commons-keyboard-key/commons-keyboard-key.component';
import { CommonsKeyboardComponent } from './components/commons-keyboard/commons-keyboard.component';
import { CommonsEditorComponent } from './components/commons-editor/commons-editor.component';
import { CommonsEditorDisplayComponent } from './components/commons-editor-display/commons-editor-display.component';
import { CommonsDrawerEditorComponent } from './components/commons-drawer-editor/commons-drawer-editor.component';

import { CommonsEditorService } from './services/commons-editor.service';

@NgModule({
		imports: [
				CommonModule,
				NgxWebAppCommonsCoreModule,
				NgxWebAppCommonsMobileModule
		],
		declarations: [
				CommonsKeyboardKeyComponent,
				CommonsKeyboardComponent,
				CommonsEditorComponent,
				CommonsEditorDisplayComponent,
				CommonsDrawerEditorComponent
		],
		exports: [
				CommonsKeyboardComponent,
				CommonsEditorComponent,
				CommonsDrawerEditorComponent
		]
})
export class NgxWebAppCommonsKeyboardModule {
	static forRoot(): ModuleWithProviders<NgxWebAppCommonsKeyboardModule> {
		return {
				ngModule: NgxWebAppCommonsKeyboardModule,
				providers: [
					CommonsEditorService
				]
		};
	}
}
