import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { ICommonsKeyboard } from '../../interfaces/icommons-keyboard';
import { ICommonsKeyboardKey } from '../../interfaces/icommons-keyboard-key';

@Component({
		selector: 'commons-keyboard',
		templateUrl: './commons-keyboard.component.html',
		styleUrls: ['./commons-keyboard.component.less']
})
export class CommonsKeyboardComponent extends CommonsComponent implements OnInit {
	@Input() keyboard!: ICommonsKeyboard;
	
	@Input() disableOk: boolean = false;
	@Input() disableCancel: boolean = false;
	@Input() disableBackspace: boolean = false;
	@Input() disableCharacters: boolean = false;
	@Input() disableThemeInvert: boolean = false;

	@Output() onCharacter: EventEmitter<string> = new EventEmitter<string>(true);
	@Output() onOk: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onCancel: EventEmitter<void> = new EventEmitter<void>(true);
	@Output() onBackspace: EventEmitter<void> = new EventEmitter<void>(true);

	set: string|undefined = undefined;
	maxColumns: number = 0;
	
	ngOnInit(): void {
		super.ngOnInit();

		this.set = this.keyboard.default;

		this.maxColumns = 0;
		for (const set of Object.keys(this.keyboard.sets)) {
			for (const row of this.keyboard.sets[set].keys) {
				let max = 0;
				for (const key of row) {
					if (key && key.span) max += key.span;
					else max++;
				}
				
				this.maxColumns = Math.max(this.maxColumns, max);
			}
		}
	}
	
	setLabels(): {} {
		const labels: {} = {};
		
		for (const setName of Object.keys(this.keyboard.sets)) {
			labels[setName] = this.keyboard.sets[setName].label;
		}
		
		return labels;
	}
	
	isDisabled(key: ICommonsKeyboardKey): boolean {
		if (key.ok && this.disableOk) return true;
		if (key.backspace && this.disableBackspace) return true;
		if (key.cancel && this.disableCancel) return true;
		if ((key.character || key.space) && this.disableCharacters) return true;
		
		return false;
	}
	
	doChangeSet(set: string): void {
		this.set = set;
	}

}
