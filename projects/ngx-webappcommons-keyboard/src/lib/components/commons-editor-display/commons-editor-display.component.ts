import { Component, OnChanges, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: 'commons-editor-display',
		templateUrl: './commons-editor-display.component.html',
		styleUrls: ['./commons-editor-display.component.less']
})
export class CommonsEditorDisplayComponent extends CommonsComponent implements OnChanges {
	@Input() pre!: string;
	@Input() post!: string;
	
	@Output() onMove: EventEmitter<number> = new EventEmitter<number>(true);
	
	preCharacters: string[] = [];
	postCharacters: string[] = [];
	
	ngOnChanges(changes: any): void {
		if (changes.pre !== undefined) this.preCharacters = changes.pre.currentValue.split('');
		if (changes.post !== undefined) this.postCharacters = changes.post.currentValue.split('');
	}

	movePre(index: number, event: Event): void {
		event.stopPropagation();
		this.onMove.emit(index);
	}
	
	movePost(index: number, event: Event): void {
		event.stopPropagation();
		this.onMove.emit(this.preCharacters.length + index);
	}
	
	moveEnd(): void {
		this.onMove.emit(-1);
	}
}
