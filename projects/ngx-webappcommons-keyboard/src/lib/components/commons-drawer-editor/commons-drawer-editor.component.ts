import { Component, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { ECommonsDrawerDirection } from 'ngx-webappcommons-mobile';

import { CommonsEditorService, TShowEditor } from '../../services/commons-editor.service';

import { ICommonsKeyboard } from '../../interfaces/icommons-keyboard';

@Component({
		selector: 'commons-drawer-editor',
		templateUrl: './commons-drawer-editor.component.html',
		styleUrls: ['./commons-drawer-editor.component.less']
})
export class CommonsDrawerEditorComponent extends CommonsComponent implements OnInit {
	ECommonsDrawerDirection = ECommonsDrawerDirection;
	
	show: boolean = false;

	value: string = '';
	allowEmpty: boolean = false;
	maxLength: number = 255;
	minLength: number = 0;
	
	keyboard: ICommonsKeyboard|undefined = undefined;
	
	// these are simply placeholders that are replaced by the subscription data, so for now don't do anything
	ok: (_: string) => void = (_: string): void => {
		// do nothing
	}
	cancel: () => void = (): void => {
		// do nothing
	}

	constructor(
			private editorService: CommonsEditorService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.editorService.showObservable(),
				(data: TShowEditor): void => {
					this.value = data.value;
					this.keyboard = data.settings.keyboard;
					this.allowEmpty = data.settings.allowEmpty || false;
					this.maxLength = data.settings.maxLength || 255;
					this.minLength = data.settings.minLength || 0;
					
					this.ok = data.ok;
					this.cancel = data.cancel;
					
					this.show = true;
				}
		);
		
		this.subscribe(
				this.editorService.hideObservable(),
				(): void => {
					this.show = false;
				}
		);
	}
	
	doCancel(): void {
		this.cancel();
	}
	
	doOk(value: string): void {
		this.ok(value);
	}
}
