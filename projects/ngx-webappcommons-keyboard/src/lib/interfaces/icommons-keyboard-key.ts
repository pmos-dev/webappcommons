export interface ICommonsKeyboardKey {
		character?: string;
		icon?: string;
		space?: boolean;
		ok?: boolean;
		cancel?: boolean;
		backspace?: boolean;
		set?: string;
		
		span?: number;
}
