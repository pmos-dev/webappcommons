export interface ICommonsKeyboard {
		default: string;
		sets: {};
}

export const FULL_KEYBOARD: ICommonsKeyboard = {
	default: 'lower',

	sets: {
		lower: { label: 'a-z', keys: [
			[
				{ cancel: true, icon: 'cancel' },
				{ character: '1' },
				{ character: '2' },
				{ character: '3' },
				{ character: '4' },
				{ character: '5' },
				{ character: '6' },
				{ character: '7' },
				{ character: '8' },
				{ character: '9' },
				{ character: '0' },
				{ backspace: true, icon: 'backspace' }
			], [
				null,
				{ character: 'q' },
				{ character: 'w' },
				{ character: 'e' },
				{ character: 'r' },
				{ character: 't' },
				{ character: 'y' },
				{ character: 'u' },
				{ character: 'i' },
				{ character: 'o' },
				{ character: 'p' },
				{ ok: true, icon: 'check_circle' }
			], [
				{ set: 'upper', span: 2 },
				{ character: 'a' },
				{ character: 's' },
				{ character: 'd' },
				{ character: 'f' },
				{ character: 'g' },
				{ character: 'h' },
				{ character: 'j' },
				{ character: 'k' },
				{ character: 'l' }
			], [
				{ set: 'symbols', span: 2 },
				{ character: 'z' },
				{ character: 'x' },
				{ character: 'c' },
				{ character: 'v' },
				{ character: 'b' },
				{ character: 'n' },
				{ character: 'm' }
			], [
				null,
				null,
				null,
				null,
				{ space: true, span: 4 }
			]
		]},
		
		upper: { label: 'A-Z', keys: [
			[
				{ cancel: true, icon: 'cancel' },
				{ character: '1' },
				{ character: '2' },
				{ character: '3' },
				{ character: '4' },
				{ character: '5' },
				{ character: '6' },
				{ character: '7' },
				{ character: '8' },
				{ character: '9' },
				{ character: '0' },
				{ backspace: true, icon: 'backspace' }
			], [
				null,
				{ character: 'Q' },
				{ character: 'W' },
				{ character: 'E' },
				{ character: 'R' },
				{ character: 'T' },
				{ character: 'Y' },
				{ character: 'U' },
				{ character: 'I' },
				{ character: 'O' },
				{ character: 'P' },
				{ ok: true, icon: 'check_circle' }
			], [
				{ set: 'lower', span: 2 },
				{ character: 'A' },
				{ character: 'S' },
				{ character: 'D' },
				{ character: 'F' },
				{ character: 'G' },
				{ character: 'H' },
				{ character: 'J' },
				{ character: 'K' },
				{ character: 'L' }
			], [
				{ set: 'symbols', span: 2 },
				{ character: 'Z' },
				{ character: 'X' },
				{ character: 'C' },
				{ character: 'V' },
				{ character: 'B' },
				{ character: 'N' },
				{ character: 'M' }
			], [
				null,
				null,
				null,
				null,
				{ space: true, span: 4 }
			]
		]},
		
		symbols: { label: '@#*', keys: [
			[
				{ cancel: true, icon: 'cancel' },
				{ character: '!' },
				{ character: '"' },
				{ character: '£' },
				{ character: '$' },
				{ character: '%' },
				{ character: '^' },
				{ character: '&' },
				{ character: '*' },
				{ character: '(' },
				{ character: ')' },
				{ backspace: true, icon: 'backspace' }
			], [
				null,
				{ character: '-' },
				{ character: '=' },
				{ character: '_' },
				{ character: '+' },
				{ character: '[' },
				{ character: ']' },
				{ character: '{' },
				{ character: '}' },
				{ character: ';' },
				{ character: '\'' },
				{ ok: true, icon: 'check_circle' }
			], [
				null,
				null,
				{ character: '#' },
				{ character: ':' },
				{ character: '@' },
				{ character: '~' },
				{ character: ',' },
				{ character: '.' },
				{ character: '/' },
				{ character: '<' },
				{ character: '>' }
			], [
				{ set: 'lower', span: 2 },
				{ character: '?' },
				{ character: '`' },
				{ character: '¬' },
				{ character: '|' }
			], [
				null,
				null,
				null,
				null,
				{ space: true, span: 4 }
			]
		]}
	}
};

export const SIMPLE_KEYBOARD: ICommonsKeyboard = {
	default: 'alphanumeric',

	sets: {
		alphanumeric: { label: 'AZ09', keys: [
			[
				{ character: '1' },
				{ character: '2' },
				{ character: '3' },
				{ character: '4' },
				{ character: '5' },
				{ character: '6' },
				{ character: '7' },
				{ character: '8' },
				{ character: '9' },
				{ character: '0' }
			], [
				{ character: 'Q' },
				{ character: 'W' },
				{ character: 'E' },
				{ character: 'R' },
				{ character: 'T' },
				{ character: 'Y' },
				{ character: 'U' },
				{ character: 'I' },
				{ character: 'O' },
				{ character: 'P' }
			], [
				{ character: 'A' },
				{ character: 'S' },
				{ character: 'D' },
				{ character: 'F' },
				{ character: 'G' },
				{ character: 'H' },
				{ character: 'J' },
				{ character: 'K' },
				{ character: 'L' }
			], [
				null,
				{ character: 'Z' },
				{ character: 'X' },
				{ character: 'C' },
				{ character: 'V' },
				{ character: 'B' },
				{ character: 'N' },
				{ character: 'M' }
			], [
				{ set: 'symbols', span: 2 },
				{ space: true, span: 4 },
				{ backspace: true, icon: 'backspace' },
				null,
				{ ok: true, icon: 'check_circle' },
				{ cancel: true, icon: 'cancel' }
			]
		]},
		
		symbols: { label: '@#*', keys: [
			[
				{ character: '!' },
				{ character: '"' },
				{ character: '£' },
				{ character: '$' },
				{ character: '%' },
				{ character: '^' },
				{ character: '&' },
				{ character: '*' },
				{ character: '(' },
				{ character: ')' }
			], [
				{ character: '-' },
				{ character: '_' },
				{ character: '+' },
				{ character: '=' },
				{ character: '[' },
				{ character: ']' },
				{ character: '{' },
				{ character: '}' }
			], [
				{ character: ';' },
				{ character: ':' },
				{ character: '\'' },
				{ character: '@' },
				{ character: '#' },
				{ character: '~' }
			], [
				{ character: ',' },
				{ character: '.' },
				{ character: '<' },
				{ character: '>' },
				{ character: '/' },
				{ character: '?' }
			], [
				{ set: 'alphanumeric', span: 2 },
				{ space: true, span: 4 },
				{ backspace: true, icon: 'backspace' },
				null,
				{ ok: true, icon: 'check_circle' },
				{ cancel: true, icon: 'cancel' }
			]
		]}
	}
};

export const LOGIN_PIN_KEYBOARD: ICommonsKeyboard = {
	default: 'numeric',

	sets: {
		numeric: { label: '', keys: [
			[
				{ character: '1' },
				{ character: '2' },
				{ character: '3' }
			], [
				{ character: '4' },
				{ character: '5' },
				{ character: '6' }
			], [
				{ character: '7' },
				{ character: '8' },
				{ character: '9' }
			], [
				{ backspace: true, icon: 'backspace' },
				{ character: '0' },
				{ ok: true, icon: 'check_circle' }
			]
		]}
	}
};
