import { ICommonsKeyboardKey } from './icommons-keyboard-key';

export interface ICommonsKeyboardSet {
		label: string;
		keys: ICommonsKeyboardKey[][];
}
