import { Component, OnInit, OnChanges, Input, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';

import { Subscription } from 'rxjs';

import { ECommonsInvocation } from 'tscommons-async';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-cylinder-picker',
	templateUrl: './commons-cylinder-picker.component.html',
	styleUrls: ['./commons-cylinder-picker.component.less']
})
export class CommonsCylinderPickerComponent extends CommonsComponent implements OnInit, OnChanges {
	@ViewChild('aside', { static: true }) aside!: ElementRef;
	@ViewChild('section', { static: true }) section!: ElementRef;
	
	@Input() items: string[] = [];
	@Input() disabled: boolean = false;

	@Input() item?: string;
	@Output() itemChange: EventEmitter<string> = new EventEmitter<string>();

	@Output() onInfinitePrev: EventEmitter<void> = new EventEmitter<void>();
	@Output() onInfiniteNext: EventEmitter<void> = new EventEmitter<void>();

	active: boolean = false;
	throwing: Subscription|undefined;
	
	px: number = 0;
	private panStartPx: number|undefined = undefined;
	
	constructor(
	) {
		super();
	}
	
	private getPxByItem(item: string): number {
		const index: number = this.items.indexOf(item);
		if (index === -1) return 0;
		
		const itemHeight: number = this.aside.nativeElement.offsetHeight;
		const visible: number = Math.round(this.section.nativeElement.offsetHeight / itemHeight);
		
		const delta = ((visible - 1) / 2) - (index + this.items.length);
		return delta * itemHeight;
	}

	private getItemByPx(): number {
		const itemHeight: number = this.aside.nativeElement.offsetHeight;
		const temp: number = Math.round(this.px / itemHeight);

		const visible: number = Math.round(this.section.nativeElement.offsetHeight / itemHeight);
		const selected: number = (((visible - 1) / 2) - temp) - this.items.length;

		return selected;
	}

	ngOnInit(): void {
		super.ngOnInit();
	
		if (this.item !== undefined) this.px = this.getPxByItem(this.item);
	}
		
	ngOnChanges(): void {
		if (this.item !== undefined) this.px = this.getPxByItem(this.item);
	}

	private setThrow(speed: number, interval: number): void {
		speed = (speed < 0 ? -1 : 1) * Math.max(Math.abs(speed), Math.abs(Math.pow(speed, 3)));
		this.throwing = this.setInterval(
				interval,
				interval,
				ECommonsInvocation.FIXED,
				async (): Promise<void> => {
					this.px += speed * interval;
					speed *= 0.95;
					if (Math.abs(speed) < 0.03) {
						this.cancelThrow();
						this.snap();
						return;
					}
				}
		);
	}
	
	private cancelThrow(): void {
		if (!this.throwing) return;
		
		this.throwing.unsubscribe();
		this.throwing = undefined;
	}
	
	private snap(): void {
		let index: number = this.getItemByPx();
		while (index < 0) {
			this.onInfinitePrev.emit();
			index += this.items.length;
		}
		while (index >= this.items.length) {
			this.onInfiniteNext.emit();
			index -= this.items.length;
		}
	
		this.itemChange.emit(this.items[index]);
		this.px = this.getPxByItem(this.items[index]);
	}
	
	doMouseDown(): boolean {
		if (this.disabled) return false;
		
		this.cancelThrow();
		
		return false;
	}
	
	doPanStart(event: HammerInput): void {
		if (this.disabled) return;
		
		this.cancelThrow();
		
		this.panStartPx = this.px;
		
		this.active = true;
		
		this.handlePan(event);
	}
	
	private handlePan(event: HammerInput): void {
		if (this.panStartPx === undefined) return;
		
		this.px = this.panStartPx + event.deltaY;
	}
	
	doPanMove(event: HammerInput) {
		if (this.disabled) return;
		if (event.direction !== 8 && event.direction !== 16) return;
		
		this.handlePan(event);
	}
	
	doPanEnd(event: HammerInput) {
		if (this.disabled) return;
		
		this.handlePan(event);

		this.panStartPx = undefined;
		this.active = false;
		
		// throw
		const speed: number = event.deltaY / event.deltaTime;
		if (Math.abs(speed) > 0.45) {
			this.setThrow(speed, 10);
			return;
		}
		
		this.snap();
	}
	
	isSelected(i: number): boolean {
		const itemHeight: number = this.aside.nativeElement.offsetHeight;
		const temp: number = Math.round(this.px / itemHeight);

		const visible: number = Math.round(this.section.nativeElement.offsetHeight / itemHeight);
		let selected: number = ((visible - 1) / 2) - temp;
		selected -= this.items.length;

		return i === selected;
	}
}
