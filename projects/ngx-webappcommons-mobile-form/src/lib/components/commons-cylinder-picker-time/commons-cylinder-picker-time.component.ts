import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-cylinder-picker-time',
	templateUrl: './commons-cylinder-picker-time.component.html',
	styleUrls: ['./commons-cylinder-picker-time.component.less']
})
export class CommonsCylinderPickerTimeComponent extends CommonsComponent implements OnInit, OnChanges {
	@Input() timestamp!: Date;
	@Output() timestampChange: EventEmitter<Date> = new EventEmitter<Date>();
	
	@Input() wrapping: boolean = true;

	hours: string[] = [];
	minutes: string[] = [];
	
	hour: string = '00';
	minute: string = '00';
	
	constructor() {
		super();
		
		this.hours = [];
		for (let i = 0; i < 24; i++) {
			let h: string = i.toString();
			if (h.length < 2) h = `0${h}`;
			this.hours.push(h);
		}
		
		this.minutes = [];
		for (let i = 0; i < 60; i++) {
			let m: string = i.toString();
			if (m.length < 2) m = `0${m}`;
			this.minutes.push(m);
		}
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.updateTime();
	}

	ngOnChanges(): void {
		this.updateTime();
	}
	
	private updateTime(): void {
		if (this.timestamp) {
			let h: string = this.timestamp.getHours().toString();
			if (h.length < 2) h = `0${h}`;
			this.hour = h;

			let m: string = this.timestamp.getMinutes().toString();
			if (m.length < 2) m = `0${m}`;
			this.minute = m;
		}
	}
	
	doTrigger(): void {
		if (!this.timestamp) return;

		const copy: Date = new Date(this.timestamp.getTime());

		copy.setHours(parseInt(this.hour, 10));
		copy.setMinutes(parseInt(this.minute, 10));
		copy.setSeconds(0);
		copy.setMilliseconds(0);
		
		this.timestampChange.emit(copy);
	}
	
	doWrapPrevDay(): void {
		if (!this.timestamp || !this.wrapping) return;

		// onInfinitePrev/Next happen before the time change emit.
		// the subsequent time change event will do the emit.

		this.timestamp.setDate(this.timestamp.getDate() - 1);
	}
	
	doWrapNextDay(): void {
		if (!this.timestamp || !this.wrapping) return;

		// onInfinitePrev/Next happen before the time change emit.
		// the subsequent time change event will do the emit.

		this.timestamp.setDate(this.timestamp.getDate() + 1);
	}
	
	doWrapPrevHour(): void {
		if (!this.timestamp || !this.wrapping) return;

		let index: number = this.hours.indexOf(this.hour);
		index--;
		
		if (index < 0) {
			index += this.hours.length;
			this.doWrapPrevDay();
		}
		
		this.hour = this.hours[index];
	}
	
	doWrapNextHour(): void {
		if (!this.timestamp || !this.wrapping) return;

		let index: number = this.hours.indexOf(this.hour);
		index++;
		
		if (index >= this.hours.length) {
			index -= this.hours.length;
			this.doWrapNextDay();
		}
		
		this.hour = this.hours[index];
	}
}
