import { Component, Input, Output, EventEmitter, OnInit, OnChanges } from '@angular/core';

import { CommonsDate } from 'tscommons-core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: 'commons-cylinder-picker-date',
		templateUrl: './commons-cylinder-picker-date.component.html',
		styleUrls: ['./commons-cylinder-picker-date.component.less']
})
export class CommonsCylinderPickerDateComponent extends CommonsComponent implements OnInit, OnChanges {
	@Input() timestamp!: Date;
	@Output() timestampChange: EventEmitter<Date> = new EventEmitter<Date>();
	
	@Input() wrapping: boolean = true;
	@Input() short: boolean = false;

	days: string[] = [];
	months: string[] = [];
	years: string[] = [];
	day: string = '';
	month: string = '';
	year: string = '';
	
	constructor() {
		super();
		
		this.years = [];
		for (let i = 2010; i <= 2029; i++) this.years.push(i.toString());
	}

	ngOnInit(): void {
		super.ngOnInit();

		this.months = CommonsDate.listTextMonths()
				.map((m: string): string => this.short ? m.substring(0, 3) : m);
		this.years = this.years
				.map((y: string): string => this.short ? y.substring(2, 4) : y);
		
		this.updateDate();
	}
	
	ngOnChanges(): void {
		this.updateDate();
	}
	
	recalculateDays(): void {
		const max: number = CommonsDate.getNumberOfDaysInMonth(CommonsDate.getNumericMonth(this.month), parseInt(this.year, 10) + (this.short ? 2000 : 0));
		this.days = CommonsDate.listTwoDigitDays(max);
		
		if (parseInt(this.day, 10) > max) this.day = this.days[max - 1];
	}
	
	private updateDate(): void {
		if (this.timestamp) {
			const formatted: string[] = CommonsDate.dateToYmd(this.timestamp).split('-');

			this.day = formatted[2];
			this.month = CommonsDate.getTextMonth(this.timestamp.getMonth());
			if (this.short) this.month = this.month.substring(0, 3);
			this.year = formatted[0];
			if (this.short) this.year = this.year.substring(2, 4);
			
			this.recalculateDays();
		}
	}
	
	doTrigger(): void {
		if (!this.timestamp) return;

		const copy: Date = new Date(this.timestamp.getTime());

		copy.setDate(parseInt(this.day, 10));
		copy.setMonth(CommonsDate.getNumericMonth(this.month));
		copy.setFullYear(parseInt(this.year, 10) + (this.short ? 2000 : 0));
		
		this.timestampChange.emit(copy);
	}
	
	doWrapPrevYear(): void {
		if (!this.timestamp || !this.wrapping) return;

		// onInfinitePrev/Next happen before the time change emit.
		// the subsequent time change event will do the emit.

		this.timestamp.setFullYear(this.timestamp.getFullYear() - 1);
	}
	
	doWrapNextYear(): void {
		if (!this.timestamp || !this.wrapping) return;

		// onInfinitePrev/Next happen before the time change emit.
		// the subsequent time change event will do the emit.

		this.timestamp.setFullYear(this.timestamp.getFullYear() + 1);
	}
	
	doWrapPrevMonth(): void {
		if (!this.timestamp || !this.wrapping) return;

		let index: number = this.months.indexOf(this.month);
		index--;
		
		if (index < 0) {
			index += this.months.length;
			this.doWrapPrevYear();
		}
		
		this.month = this.months[index];
	}
	
	doWrapNextMonth(): void {
		if (!this.timestamp || !this.wrapping) return;

		let index: number = this.months.indexOf(this.month);
		index++;
		
		if (index >= this.months.length) {
			index -= this.months.length;
			this.doWrapNextYear();
		}
		
		this.month = this.months[index];
	}
	
	doWrapPrevDay(): void {
		if (!this.timestamp || !this.wrapping) return;

		let index: number = this.days.indexOf(this.day);
		index--;
		
		if (index < 0) {
			index += this.days.length;
			this.doWrapPrevMonth();
		}
		
		this.day = this.days[index];
	}
	
	doWrapNextDay(): void {
		if (!this.timestamp || !this.wrapping) return;

		let index: number = this.days.indexOf(this.day);
		index++;
		
		if (index >= this.days.length) {
			index -= this.days.length;
			this.doWrapNextMonth();
		}
		
		this.day = this.days[index];
	}
}
