import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsSelectorService } from '../../services/commons-selector.service';

@Component({
		selector: 'commons-drawer-dropdown',
		templateUrl: './commons-drawer-dropdown.component.html',
		styleUrls: ['./commons-drawer-dropdown.component.less']
})
export class CommonsDrawerDropdownComponent extends CommonsComponent {
	@Input() disabled: boolean = false;
	@Input() placeholder?: string;
	@Input() options: string[] = [];
	@Input() allowNone: boolean = false;
	
	@Output() valueChange: EventEmitter<string> = new EventEmitter<string>();
	@Input() value: string|undefined;

	constructor(
			private selectorService: CommonsSelectorService
	) {
		super();
	}

	doEdit(): void {
		this.selectorService.select(
				this.value,
				this.options,
				{
					allowNone: this.allowNone
				},
				(option: string): void => {
					this.valueChange.emit(option);
				},
				(): void => {
					// do nothing
				}
		);
	}
}
