import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';

import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsFormModule } from 'ngx-webappcommons-form';
import { NgxWebAppCommonsKeyboardModule } from 'ngx-webappcommons-keyboard';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';

import { CommonsDrawerTextboxComponent } from './components/commons-drawer-textbox/commons-drawer-textbox.component';
import { CommonsDrawerDropdownComponent } from './components/commons-drawer-dropdown/commons-drawer-dropdown.component';
import { CommonsDrawerSelectorComponent } from './components/commons-drawer-selector/commons-drawer-selector.component';
import { CommonsCylinderPickerComponent } from './components/commons-cylinder-picker/commons-cylinder-picker.component';
import { CommonsCylinderPickerTimeComponent } from './components/commons-cylinder-picker-time/commons-cylinder-picker-time.component';
import { CommonsCylinderPickerDateComponent } from './components/commons-cylinder-picker-date/commons-cylinder-picker-date.component';
import { CommonsCircularPickerComponent } from './components/commons-circular-picker/commons-circular-picker.component';
import { CommonsDialogTimeComponent } from './components/commons-dialog-time/commons-dialog-time.component';
import { CommonsDateTimeComponent } from './components/commons-date-time/commons-date-time.component';

import { CommonsSelectorService } from './services/commons-selector.service';
import { CommonsDialogTimeService } from './services/commons-dialog-time.service';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsPipeModule,
				NgxWebAppCommonsAppModule,
				NgxWebAppCommonsFormModule,
				NgxWebAppCommonsKeyboardModule,
				NgxWebAppCommonsMobileModule
		],
		declarations: [
				CommonsDrawerTextboxComponent,
				CommonsDrawerDropdownComponent,
				CommonsDrawerSelectorComponent,
				CommonsCylinderPickerComponent,
				CommonsCylinderPickerTimeComponent,
				CommonsCylinderPickerDateComponent,
				CommonsCircularPickerComponent,
				CommonsDialogTimeComponent,
				CommonsDateTimeComponent
		],
		exports: [
				CommonsDrawerTextboxComponent,
				CommonsDrawerDropdownComponent,
				CommonsDrawerSelectorComponent,
				CommonsCylinderPickerComponent,
				CommonsCylinderPickerTimeComponent,
				CommonsCylinderPickerDateComponent,
				CommonsCircularPickerComponent,
				CommonsDialogTimeComponent,
				CommonsDateTimeComponent
		]
})
export class NgxWebAppCommonsMobileFormModule {
	static forRoot(): ModuleWithProviders<NgxWebAppCommonsMobileFormModule> {
		return {
				ngModule: NgxWebAppCommonsMobileFormModule,
				providers: [
						CommonsSelectorService,
						CommonsDialogTimeService
				]
		};
	}
}
