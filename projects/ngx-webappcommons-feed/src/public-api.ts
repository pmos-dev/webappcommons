/*
 * Public API Surface of ngx-webappcommons-feed
 */

export * from './lib/interfaces/icommons-post';

export * from './lib/components/commons-avatar-post-timestamp/commons-avatar-post-timestamp.component';
export * from './lib/components/commons-avatar-post/commons-avatar-post.component';
export * from './lib/components/commons-avatar-feed/commons-avatar-feed.component';

export * from './lib/ngx-webappcommons-feed.module';
