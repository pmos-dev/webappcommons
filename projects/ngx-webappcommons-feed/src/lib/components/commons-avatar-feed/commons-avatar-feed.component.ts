import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { ICommonsPost } from '../../interfaces/icommons-post';

@Component({
	selector: 'commons-avatar-feed',
	templateUrl: './commons-avatar-feed.component.html',
	styleUrls: ['./commons-avatar-feed.component.less']
})
export class CommonsAvatarFeedComponent extends CommonsComponent {
	@Input() title!: string;
	@Input() absolute: boolean = false;
	@Input() autoSize: boolean = false;
	@Input() truncate?: number;
	
	@Input() posts: ICommonsPost[] = [];
	
	@Output() onSelect: EventEmitter<ICommonsPost> = new EventEmitter<ICommonsPost>();

	doSelect(post: ICommonsPost): void {
		this.onSelect.emit(post);
	}
}
