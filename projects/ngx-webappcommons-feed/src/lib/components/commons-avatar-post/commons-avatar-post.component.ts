import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { ICommonsPost } from '../../interfaces/icommons-post';

@Component({
	selector: 'commons-avatar-post',
	templateUrl: './commons-avatar-post.component.html',
	styleUrls: ['./commons-avatar-post.component.less']
})
export class CommonsAvatarPostComponent extends CommonsComponent {
	@Input() post!: ICommonsPost;
	@Input() absolute!: boolean;
	@Input() truncate?: number;
	
	@Output() onSelect: EventEmitter<void> = new EventEmitter<void>();

	doSelect(): void {
		this.onSelect.emit();
	}
}
