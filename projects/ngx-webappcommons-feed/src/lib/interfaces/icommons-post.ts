import { ICommonsAvatar } from 'ngx-webappcommons-app';

export interface ICommonsPost {
		id?: any;
		
		avatar: ICommonsAvatar;
		
		name: string;
		label?: string;
		
		message: string;
		time: Date;
}
