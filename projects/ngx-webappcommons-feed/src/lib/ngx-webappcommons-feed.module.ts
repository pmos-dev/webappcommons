import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';

import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';

import { CommonsAvatarFeedComponent } from './components/commons-avatar-feed/commons-avatar-feed.component';
import { CommonsAvatarPostComponent } from './components/commons-avatar-post/commons-avatar-post.component';
import { CommonsAvatarPostTimestampComponent } from './components/commons-avatar-post-timestamp/commons-avatar-post-timestamp.component';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsPipeModule,
				NgxWebAppCommonsAppModule
		],
		declarations: [
				CommonsAvatarFeedComponent,
				CommonsAvatarPostComponent,
				CommonsAvatarPostTimestampComponent
		],
		exports: [
				CommonsAvatarFeedComponent
		]
})
export class NgxWebAppCommonsFeedModule { }
