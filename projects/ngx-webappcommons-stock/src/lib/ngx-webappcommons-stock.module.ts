import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxAngularCommonsPipeModule } from 'ngx-angularcommons-pipe';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsFormModule } from 'ngx-webappcommons-form';

import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { CommonsThemeSelectComponent } from './components/commons-theme-select/commons-theme-select.component';
import { CommonsFigureTallySummaryComponent } from './components/commons-figure-tally-summary/commons-figure-tally-summary.component';
import { CommonsFigureTallyBarComponent } from './components/commons-figure-tally-bar/commons-figure-tally-bar.component';

@NgModule({
		imports: [
				CommonModule,
				NgxAngularCommonsPipeModule,
				NgxWebAppCommonsCoreModule,
				NgxWebAppCommonsAppModule,
				NgxWebAppCommonsFormModule
		],
		declarations: [
				PageNotFoundComponent,
				CommonsThemeSelectComponent,
				CommonsFigureTallySummaryComponent,
				CommonsFigureTallyBarComponent
		],
		exports: [
				CommonsThemeSelectComponent,
				CommonsFigureTallySummaryComponent,
				CommonsFigureTallyBarComponent
		]
})
export class NgxWebAppCommonsStockModule {
	static forRoot(): ModuleWithProviders<NgxWebAppCommonsStockModule> {
		return {
				ngModule: NgxWebAppCommonsStockModule,
				providers: [
				]
		};
	}
}
