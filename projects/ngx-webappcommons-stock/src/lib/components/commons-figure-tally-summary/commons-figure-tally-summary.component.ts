import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-figure-tally-summary',
	templateUrl: './commons-figure-tally-summary.component.html',
	styleUrls: ['./commons-figure-tally-summary.component.less']
})
export class CommonsFigureTallySummaryComponent extends CommonsComponent {
	@Input() label: string = '';
	@Input() tally: number|undefined;
	@Input() leftLabel: string|undefined;
	@Input() left: number|undefined;
	@Input() leftPercent: number|undefined;
	@Input() rightLabel: string|undefined;
	@Input() right: number|undefined;
	@Input() rightPercent: number|undefined;
	
	constructor() {
		super();
	}

}
