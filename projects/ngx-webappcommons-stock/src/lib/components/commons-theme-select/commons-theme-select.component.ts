import { Component, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsThemeService } from 'ngx-webappcommons-core';
import { ECommonsThemeColor, ECOMMONS_THEME_COLORS } from 'ngx-webappcommons-core';
import { ECommonsThemeContrast } from 'ngx-webappcommons-core';

@Component({
		selector: 'commons-theme-select',
		templateUrl: './commons-theme-select.component.html',
		styleUrls: ['./commons-theme-select.component.less']
})
export class CommonsThemeSelectComponent extends CommonsComponent implements OnInit {
	ECommonsThemeColor = ECommonsThemeColor;
	ECommonsThemeContrast = ECommonsThemeContrast;
	ECOMMONS_THEME_COLORS = ECOMMONS_THEME_COLORS;

	color: ECommonsThemeColor|undefined;
	contrast: ECommonsThemeContrast|undefined;

	constructor(
			private themeService: CommonsThemeService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.subscribe(
				this.themeService.colorChangedObservable(),
				(c: ECommonsThemeColor|undefined): void => {
					this.color = c;
				}
		);
		
		this.subscribe(
				this.themeService.contrastChangedObservable(),
				(c: ECommonsThemeContrast|undefined): void => {
					this.contrast = c;
				}
		);
	}

	doTheme(): void {
		if (this.color !== undefined) {
			this.themeService.setCurrentColor(this.color);
		}
		if (this.contrast !== undefined) {
			this.themeService.setCurrentContrast(this.contrast);
		}
	}

}
