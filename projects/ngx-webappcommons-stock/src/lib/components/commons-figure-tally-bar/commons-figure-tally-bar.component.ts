import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: 'commons-figure-tally-bar',
		templateUrl: './commons-figure-tally-bar.component.html',
		styleUrls: ['./commons-figure-tally-bar.component.less']
})
export class CommonsFigureTallyBarComponent extends CommonsComponent {
	@Input() labels: string[] = [];
	@Input() tallies: Map<string, number|undefined> = new Map<string, number|undefined>();

}
