import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-pin-login',
	templateUrl: './commons-pin-login.component.html',
	styleUrls: ['./commons-pin-login.component.less']
})
export class CommonsPinLoginComponent extends CommonsComponent {
	@Input() minLength: number = 1;
	@Input() maxLength: number = 4;
	@Input() title?: string;
	@Output() onLogin: EventEmitter<string> = new EventEmitter<string>(true);

	constructor(
	) {
		super();
	}
	
	doLogin(pin: string): void {
		this.onLogin.emit(pin);
	}
}
