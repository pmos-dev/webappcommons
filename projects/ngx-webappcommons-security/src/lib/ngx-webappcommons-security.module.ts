import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgxWebAppCommonsAppModule } from 'ngx-webappcommons-app';
import { NgxWebAppCommonsMobileModule } from 'ngx-webappcommons-mobile';
import { NgxWebAppCommonsKeyboardModule } from 'ngx-webappcommons-keyboard';

import { CommonsPinLockerComponent } from './components/commons-pin-locker/commons-pin-locker.component';
import { CommonsPinComponent } from './components/commons-pin/commons-pin.component';
import { CommonsPinLoginComponent } from './components/commons-pin-login/commons-pin-login.component';

import { CommonsLockService } from './services/commons-lock.service';

@NgModule({
		imports: [
				CommonModule,
				NgxWebAppCommonsAppModule,
				NgxWebAppCommonsMobileModule,
				NgxWebAppCommonsKeyboardModule
		],
		declarations: [
				CommonsPinComponent,
				CommonsPinLockerComponent,
				CommonsPinLoginComponent
		],
		exports: [
				CommonsPinComponent,
				CommonsPinLockerComponent,
				CommonsPinLoginComponent
		]
})
export class NgxWebAppCommonsSecurityModule {
	static forRoot(): ModuleWithProviders<NgxWebAppCommonsSecurityModule> {
		return {
				ngModule: NgxWebAppCommonsSecurityModule,
				providers: [
						CommonsLockService
				]
		};
	}
}
