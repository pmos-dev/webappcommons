import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsArray } from 'tscommons-core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-chips-filter',
	templateUrl: './commons-chips-filter.component.html',
	styleUrls: ['./commons-chips-filter.component.less']
})
export class CommonsChipsFilterComponent extends CommonsComponent {
	@Input() title: string|undefined;
	@Input() filters: string[] = [];

	@Input() selected: string[] = [];
	@Output() selectedChange: EventEmitter<string[]> = new EventEmitter<string[]>(true);
	
	constructor() {
		super();
	}

	doToggle(chip: string): void {
		if (!this.selected.includes(chip)) {
			this.selected.push(chip);
		} else {
			CommonsArray.remove<string>(this.selected, chip);
		}

		this.selectedChange.emit(this.selected);
	}
}
