import { Component, Input, Output, EventEmitter } from '@angular/core';
import { DomSanitizer, SafeStyle } from '@angular/platform-browser';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { EMaterialDesignIcons } from 'ngx-webappcommons-core';

import { ICommonsAvatar } from '../../interfaces/icommons-avatar';

const USE_IMG_RATHER_THAN_LABEL_BACKGROUND_BUG: boolean = true;

@Component({
		selector: '[commons-avatar-list-item]',
		templateUrl: './commons-avatar-list-item.component.html',
		styleUrls: ['./commons-avatar-list-item.component.less']
})
export class CommonsAvatarListItemComponent extends CommonsComponent {
	USE_IMG_RATHER_THAN_LABEL_BACKGROUND_BUG = USE_IMG_RATHER_THAN_LABEL_BACKGROUND_BUG;
	EMaterialDesignIcons = EMaterialDesignIcons;
	
	@Input() avatar!: ICommonsAvatar;
	
	@Output() onSelect: EventEmitter<void> = new EventEmitter<void>();

	constructor(
			private sanitized: DomSanitizer
	) {
		super();
	}

	doSelect(): void {
		this.onSelect.emit();
	}
	
	getIconSet(): EMaterialDesignIcons {
		return this.avatar.iconSet || EMaterialDesignIcons.MATERIALDESIGN;
	}
	
	getUrl(): SafeStyle {
		return this.sanitized.bypassSecurityTrustStyle(`url('${this.avatar.image}')`);
	}
}
