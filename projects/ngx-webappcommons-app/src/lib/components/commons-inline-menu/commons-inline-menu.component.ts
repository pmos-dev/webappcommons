import { Component, Input, HostListener, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

import { CommonsMenuService } from '../../services/commons-menu.service';

@Component({
	selector: 'commons-inline-menu',
	templateUrl: './commons-inline-menu.component.html',
	styleUrls: ['./commons-inline-menu.component.less']
})
export class CommonsInlineMenuComponent extends CommonsComponent implements OnInit {
	@Input() name!: string;
	
	@HostListener('click') closeOverflowA() {
		this.menuService.suppressBriefly(this.name);
	}

	@HostListener('document:click') closeOverflowB() {
		this.menuService.hide(this.name);
	}

	constructor(
			private menuService: CommonsMenuService
	) {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		this.menuService.suppressBriefly(this.name);
	}

}
