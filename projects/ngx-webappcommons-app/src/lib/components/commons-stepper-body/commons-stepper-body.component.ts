import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: '[commons-stepper-body]',
	templateUrl: './commons-stepper-body.component.html',
	styleUrls: ['./commons-stepper-body.component.less']
})
export class CommonsStepperBodyComponent extends CommonsComponent {

	constructor() {
		super();
	}

}
