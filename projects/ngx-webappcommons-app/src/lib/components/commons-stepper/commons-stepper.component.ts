import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: '[commons-stepper]',
	templateUrl: './commons-stepper.component.html',
	styleUrls: ['./commons-stepper.component.less']
})
export class CommonsStepperComponent extends CommonsComponent {
}
