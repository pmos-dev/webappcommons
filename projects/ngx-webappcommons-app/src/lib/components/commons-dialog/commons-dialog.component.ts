import { Component, ViewChild, ElementRef } from '@angular/core';
import { Input, Output, EventEmitter } from '@angular/core';
import { OnInit, AfterViewInit } from '@angular/core';

import { ECommonsInvocation } from 'tscommons-async';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: '[commons-dialog]',
	templateUrl: './commons-dialog.component.html',
	styleUrls: ['./commons-dialog.component.less']
})
export class CommonsDialogComponent extends CommonsComponent implements OnInit, AfterViewInit {
	@ViewChild('buttons') private buttonsElement: ElementRef<HTMLElement>;

	@Input() buttons: string[] = [];
	@Input() countdown?: number;
	@Input() defaultButton: string|undefined;
	@Input() disabledButtons: string[] = [];
	
	@Output() onButton: EventEmitter<string> = new EventEmitter<string>();
	@Output() onClickOff: EventEmitter<void> = new EventEmitter<void>();

	preDeleteButtons: string[] = [];
	postDeleteButtons: string[] = [];
	hasDeleteButton: boolean = false;
	
	constructor() {
		super();
	}

	ngOnInit(): void {
		super.ngOnInit();
		
		const index: number = this.buttons.indexOf('_delete');
		
		this.hasDeleteButton = index > -1;
		
		if (!this.hasDeleteButton) {
			this.preDeleteButtons = this.buttons.slice();
			this.postDeleteButtons = [];
		} else {
			this.preDeleteButtons = this.buttons.slice(0, index);
			this.postDeleteButtons = this.buttons.slice(index + 1);
		}
		
		this.setInterval(
				1000,
				1000,
				ECommonsInvocation.FIXED,
				async (): Promise<void> => {
					if (this.countdown === undefined) return;
			
					if (this.countdown > 0) this.countdown--;
				}
		);
	}
	
	ngAfterViewInit(): void {
		if (!this.buttonsElement) return;

		this.buttonsElement.nativeElement
				.querySelectorAll('button')
				.forEach((button: HTMLButtonElement): void => {
					if (button.classList.contains('is-default')) {
						setTimeout((): void => {
							button.focus();
						}, 10);
					}
				});
	}
	
	isButtonDisabled(button: string): boolean {
		if (this.disabledButtons.includes(button)) return true;
		
		if ((button === 'OK' || button === '_delete' || button === 'Yes') && this.countdown !== undefined && this.countdown > 0) return true;
		
		return false;
	}

	doClickOff(): void {
		this.onClickOff.emit();
	}
}
