import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-expansion-panel',
	templateUrl: './commons-expansion-panel.component.html',
	styleUrls: ['./commons-expansion-panel.component.less']
})
export class CommonsExpansionPanelComponent extends CommonsComponent {
	@Input() expanded: boolean = false;

	constructor() {
		super();
	}

}
