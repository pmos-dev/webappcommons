import { Component, Input, OnInit } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';
import { CommonsLoadingService } from 'ngx-angularcommons-app';

@Component({
	selector: 'commons-loading',
	templateUrl: './commons-loading.component.html',
	styleUrls: ['./commons-loading.component.less']
})
export class CommonsLoadingComponent extends CommonsComponent implements OnInit {
	@Input() message: string = 'Loading...';
	
	loading: boolean = true;

	constructor(
			private loadingService: CommonsLoadingService
	) {
		super();
	}

	ngOnInit() {
		this.subscribe(
				this.loadingService.loadedObservable(),
				(): void => {
					this.loading = false;
				}
		);
	}

}
