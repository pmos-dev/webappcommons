import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-menu',
	templateUrl: './commons-menu.component.html',
	styleUrls: ['./commons-menu.component.less']
})
export class CommonsMenuComponent extends CommonsComponent {
}
