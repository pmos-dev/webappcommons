import { Component, Input, Output, EventEmitter } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-modal-progress',
	templateUrl: './commons-modal-progress.component.html',
	styleUrls: ['./commons-modal-progress.component.less']
})
export class CommonsModalProgressComponent extends CommonsComponent {
	@Input() message?: string;
	@Input() icon?: string;
	@Input() rainbow?: boolean;
	@Input() cancel: boolean = false;
	
	@Output() onCancel: EventEmitter<void> = new EventEmitter<void>();

	doCancel(): void {
		this.onCancel.emit();
	}
}
