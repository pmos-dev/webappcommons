import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: '[commons-avatar-list]',
		templateUrl: './commons-avatar-list.component.html',
		styleUrls: ['./commons-avatar-list.component.less']
})
export class CommonsAvatarListComponent extends CommonsComponent {
	@Input() title: string = '';
	@Input() noTopGap: boolean = false;
	@Input() noBottomGap: boolean = false;
	@Input() biggerTopGap: boolean = false;
}
