import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: 'commons-checkbox-list',
	templateUrl: './commons-checkbox-list.component.html',
	styleUrls: ['./commons-checkbox-list.component.less']
})
export class CommonsCheckboxListComponent extends CommonsComponent {
}
