import { Component } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
	selector: '[commons-expansion-panel-content]',
	templateUrl: './commons-expansion-panel-content.component.html',
	styleUrls: ['./commons-expansion-panel-content.component.less']
})
export class CommonsExpansionPanelContentComponent extends CommonsComponent {

	constructor() {
		super();
	}

}
