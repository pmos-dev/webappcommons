import { CommonsType } from 'tscommons-core';

export enum ECommonsSnackType {
		NOTIFICATION = 'notification',
		ERROR = 'error',
		SUCCESS = 'success'
}

export function toECommonsSnackType(type: string): ECommonsSnackType|undefined {
	switch (type) {
		case ECommonsSnackType.NOTIFICATION.toString():
			return ECommonsSnackType.NOTIFICATION;
		case ECommonsSnackType.ERROR.toString():
			return ECommonsSnackType.ERROR;
		case ECommonsSnackType.SUCCESS.toString():
			return ECommonsSnackType.SUCCESS;
	}
	return undefined;
}

export function fromECommonsSnackType(type: ECommonsSnackType): string {
	switch (type) {
		case ECommonsSnackType.NOTIFICATION:
			return ECommonsSnackType.NOTIFICATION.toString();
		case ECommonsSnackType.ERROR:
			return ECommonsSnackType.ERROR.toString();
		case ECommonsSnackType.SUCCESS:
			return ECommonsSnackType.SUCCESS.toString();
	}
	
	throw new Error('Unknown ECommonsSnackType');
}

export function isECommonsSnackType(test: unknown): test is ECommonsSnackType {
	if (!CommonsType.isString(test)) return false;
	
	return toECommonsSnackType(test) !== undefined;
}

export function keyToECommonsSnackType(key: string): ECommonsSnackType {
	switch (key) {
		case 'NOTIFICATION':
			return ECommonsSnackType.NOTIFICATION;
		case 'ERROR':
			return ECommonsSnackType.ERROR;
		case 'SUCCESS':
			return ECommonsSnackType.SUCCESS;
	}
	
	throw new Error(`Unable to obtain ECommonsSnackType for key: ${key}`);
}

export const ECOMMONS_SNACK_TYPES: ECommonsSnackType[] = Object.keys(ECommonsSnackType)
		.map((e: string): ECommonsSnackType => keyToECommonsSnackType(e));
