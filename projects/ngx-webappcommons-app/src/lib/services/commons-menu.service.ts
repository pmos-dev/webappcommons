import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable()
export class CommonsMenuService {
	private showEmitter: EventEmitter<string> = new EventEmitter<string>(true);
	private hideEmitter: EventEmitter<string> = new EventEmitter<string>(true);

	private suppress: Map<string, boolean>;
	
	constructor() {
		this.suppress = new Map<string, boolean>();
	}

	public show(name: string) {
		this.showEmitter.emit(name);
	}

	public hide(name: string) {
		if (this.suppress.get(name)) return;
		
		this.hideEmitter.emit(name);
	}
	
	public showObservable(): Observable<string> {
		return this.showEmitter;
	}
	
	public hideObservable(): Observable<string> {
		return this.hideEmitter;
	}

	public suppressBriefly(name: string): void {
		this.suppress.set(name, true);
		setTimeout((): void => { this.suppress.set(name, false); }, 200);
	}
}
