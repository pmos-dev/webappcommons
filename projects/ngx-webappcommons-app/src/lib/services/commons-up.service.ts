import { Injectable, EventEmitter } from '@angular/core';

import { Observable } from 'rxjs';

@Injectable()
export class CommonsUpService {
	private titleSubject: EventEmitter<string|null> = new EventEmitter<string|null>(true);
	
	private callback: (() => void)|undefined;
	
	// only the app.component or similar should subscribe to this one.
	public titleObservable(): Observable<string|null> {
		return this.titleSubject;
	}
	
	public down(
			title: string,
			callback: () => void
	) {
		// only supporting one-level flat at the moment
		if (this.callback !== undefined) return;
		
		this.callback = callback;

		this.titleSubject.emit(title);
	}
	
	public up(): void {
		this.callback();
		
		this.callback = undefined;
	}
	
	public reset(): void {
		this.titleSubject.emit(undefined);
		this.callback = undefined;
	}
}
