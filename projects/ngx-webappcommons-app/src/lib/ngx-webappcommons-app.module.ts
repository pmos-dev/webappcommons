import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { NgxWebAppCommonsCoreModule } from 'ngx-webappcommons-core';
import { NgxWebAppCommonsGraphicsModule } from 'ngx-webappcommons-graphics';

import { CommonsMenuComponent } from './components/commons-menu/commons-menu.component';
import { CommonsModalProgressComponent } from './components/commons-modal-progress/commons-modal-progress.component';
import { CommonsDelayedComponent } from './components/commons-delayed/commons-delayed.component';
import { CommonsSnackComponent } from './components/commons-snack/commons-snack.component';
import { CommonsDialogComponent } from './components/commons-dialog/commons-dialog.component';
import { CommonsDialogsComponent } from './components/commons-dialogs/commons-dialogs.component';
import { CommonsInlineMenuComponent } from './components/commons-inline-menu/commons-inline-menu.component';
import { CommonsInlineMenuItemComponent } from './components/commons-inline-menu-item/commons-inline-menu-item.component';
import { CommonsMenuDividerComponent } from './components/commons-menu-divider/commons-menu-divider.component';
import { CommonsAvatarListItemComponent } from './components/commons-avatar-list-item/commons-avatar-list-item.component';
import { CommonsAvatarListComponent } from './components/commons-avatar-list/commons-avatar-list.component';
import { CommonsCheckboxListComponent } from './components/commons-checkbox-list/commons-checkbox-list.component';
import { CommonsSearchComponent } from './components/commons-search/commons-search.component';
import { CommonsStepperComponent } from './components/commons-stepper/commons-stepper.component';
import { CommonsStepperTitleComponent } from './components/commons-stepper-title/commons-stepper-title.component';
import { CommonsStepperBodyComponent } from './components/commons-stepper-body/commons-stepper-body.component';
import { CommonsLoadingComponent } from './components/commons-loading/commons-loading.component';
import { CommonsChipComponent } from './components/commons-chip/commons-chip.component';
import { CommonsChipsFilterComponent } from './components/commons-chips-filter/commons-chips-filter.component';
import { CommonsExpansionPanelComponent } from './components/commons-expansion-panel/commons-expansion-panel.component';
import { CommonsExpansionListComponent } from './components/commons-expansion-list/commons-expansion-list.component';
import { CommonsExpansionPanelTitleComponent } from './components/commons-expansion-panel-title/commons-expansion-panel-title.component';
import { CommonsExpansionPanelContentComponent } from './components/commons-expansion-panel-content/commons-expansion-panel-content.component';

import { CommonsUpService } from './services/commons-up.service';
import { CommonsDialogService } from './services/commons-dialog.service';
import { CommonsMenuService } from './services/commons-menu.service';

@NgModule({
		imports: [
				CommonModule,
				FormsModule,
				HttpClientModule,
				NgxWebAppCommonsCoreModule,
				NgxWebAppCommonsGraphicsModule
		],
		declarations: [
				CommonsMenuComponent,
				CommonsMenuDividerComponent,
				CommonsInlineMenuComponent,
				CommonsInlineMenuItemComponent,
				CommonsModalProgressComponent,
				CommonsDelayedComponent,
				CommonsSnackComponent,
				CommonsDialogComponent,
				CommonsDialogsComponent,
				CommonsAvatarListItemComponent,
				CommonsAvatarListComponent,
				CommonsCheckboxListComponent,
				CommonsSearchComponent,
				CommonsStepperComponent,
				CommonsStepperTitleComponent,
				CommonsStepperBodyComponent,
				CommonsLoadingComponent,
				CommonsChipComponent,
				CommonsChipsFilterComponent,
				CommonsExpansionPanelComponent,
				CommonsExpansionListComponent,
				CommonsExpansionPanelTitleComponent,
				CommonsExpansionPanelContentComponent
		],
		exports: [
				CommonsMenuComponent,
				CommonsMenuDividerComponent,
				CommonsInlineMenuComponent,
				CommonsInlineMenuItemComponent,
				CommonsModalProgressComponent,
				CommonsDelayedComponent,
				CommonsDialogComponent,
				CommonsDialogsComponent,
				CommonsSnackComponent,
				CommonsAvatarListItemComponent,
				CommonsAvatarListComponent,
				CommonsCheckboxListComponent,
				CommonsSearchComponent,
				CommonsStepperComponent,
				CommonsStepperTitleComponent,
				CommonsStepperBodyComponent,
				CommonsLoadingComponent,
				CommonsChipComponent,
				CommonsChipsFilterComponent,
				CommonsExpansionPanelComponent,
				CommonsExpansionListComponent,
				CommonsExpansionPanelTitleComponent,
				CommonsExpansionPanelContentComponent
		]
})
export class NgxWebAppCommonsAppModule {
	static forRoot(): ModuleWithProviders<NgxWebAppCommonsAppModule> {
		return {
				ngModule: NgxWebAppCommonsAppModule,
				providers: [
						CommonsUpService,
						CommonsDialogService,
						CommonsMenuService
				]
		};
	}
}
