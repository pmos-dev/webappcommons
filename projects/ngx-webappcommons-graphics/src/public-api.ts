/*
 * Public API Surface of ngx-webappcommons-graphics
 */

export * from './lib/components/commons-wait-rotate/commons-wait-rotate.component';

export * from './lib/ngx-webappcommons-graphics.module';
