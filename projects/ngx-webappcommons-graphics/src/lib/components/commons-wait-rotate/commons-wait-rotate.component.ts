import { Component, Input } from '@angular/core';

import { CommonsComponent } from 'ngx-angularcommons-core';

@Component({
		selector: 'commons-wait-rotate',
		templateUrl: './commons-wait-rotate.component.html',
		styleUrls: ['./commons-wait-rotate.component.less']
})
export class CommonsWaitRotateComponent extends CommonsComponent {
	@Input() thickness: number = 2;
}
